/**
 * 
 * FieldValidator.java 
 *
 * @author TCS
 * @Version 1.0 - 2016
 * 
 * @Copyright 2016 Tata Consultancy 
 */
package com.tcs.dess.utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

@Component
public class FieldValidator {

	//user fields
	public static final String UserT_userId = "UserT_userId";
	public static final String UserT_userName = "UserT_userName";
	public static final String UserT_tempPassword= "UserT_tempPassword";
	public static final String UserT_baseLocation = "UserT_baseLocation";
	public static final String UserT_supervisorUserId = "UserT_supervisorUserId";
	public static final String UserT_supervisorUserName = "UserT_supervisorUserName";
	public static final String UserT_userTelephone = "UserT_userTelephone";
	public static final String UserT_userEmailId = "UserT_userEmailId";
	public static final String UserT_userRole = "UserT_userRole";
	public static final String UserT_userGroup = "UserT_userGroup";
	
	private static final Map<String,Integer>FIELD_LENGTH_MAP;
	public static final Map<String,Integer>FIELD_INDEX_MAP;
	
	
	static{
	  Map<String,Integer> fieldLengthMap = new HashMap<String,Integer>();
	  fieldLengthMap.put(UserT_userId,10);
	  fieldLengthMap.put(UserT_userName, 50);
	  fieldLengthMap.put(UserT_tempPassword, 20);
	  fieldLengthMap.put(UserT_baseLocation, 50);
	  fieldLengthMap.put(UserT_supervisorUserId, 10);
	  fieldLengthMap.put(UserT_supervisorUserName, 50);
	  fieldLengthMap.put(UserT_userTelephone,20);
	  fieldLengthMap.put(UserT_userEmailId,60);
	  fieldLengthMap.put(UserT_userRole, 30);
	  fieldLengthMap.put(UserT_userGroup, 21);
	 
	  FIELD_LENGTH_MAP = Collections.unmodifiableMap(fieldLengthMap);
	  Map<String,Integer> fieldIndexMap = new HashMap<String,Integer>();
	  fieldIndexMap.put(UserT_userId,1);
	  fieldIndexMap.put(UserT_userName, 2);
	  fieldIndexMap.put(UserT_tempPassword, 3);
	  fieldIndexMap.put(UserT_baseLocation, 6);
	  fieldIndexMap.put(UserT_supervisorUserId, 10);
	  fieldIndexMap.put(UserT_supervisorUserName, 11);
	  fieldIndexMap.put(UserT_userTelephone,8);
	  fieldIndexMap.put(UserT_userEmailId,9);
	  fieldIndexMap.put(UserT_userRole, 5);
	  fieldIndexMap.put(UserT_userGroup, 4);
	  
	  FIELD_INDEX_MAP = Collections.unmodifiableMap(fieldIndexMap);
	}
	
	
	
	public static List<String> validate(String key,String value,boolean isMandatory){
		List<String> error = new ArrayList<String>();
		if(StringUtils.isEmpty(value)){
			if(isMandatory){
			 error.add("Missing value for " + key);
			}
		}else if(isExceedsLength(key,value)){
			error.add(key + " length exceeds " + value);
		}
		return error;
	}
	
	public static boolean isExceedsLength(String key,String value){
		if(value.length() > FIELD_LENGTH_MAP.get(key)){
			return true;
		}
		return false;
	}
	
	
	
}
