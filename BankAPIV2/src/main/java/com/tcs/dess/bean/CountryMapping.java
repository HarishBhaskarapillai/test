package com.tcs.dess.bean;

import java.io.Serializable;

import javax.persistence.*;

import org.hibernate.annotations.Fetch;

import com.fasterxml.jackson.annotation.JsonFilter;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the country_mapping database table.
 * 
 */
@Entity
@JsonFilter("BANKAPI")
//@JsonIdentityInfo(generator=ObjectIdGenerators.IntSequenceGenerator.class, property="countryId")
@Table(name="country_mapping")
@NamedQuery(name="CountryMapping.findAll", query="SELECT c FROM CountryMapping c")
public class CountryMapping implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="country_id")
	private Long countryId;

	private String code;

	private String name;
	
	private String description;
	
	@Column(name="created_timestamp")
	private Timestamp createdTimestamp;

	@Column(name="created_user")
	private String createdUser;

	

	@Column(name="last_updated_timestamp")
	private Timestamp lastUpdatedTimestamp;

	@Column(name="last_updated_user")
	private String lastUpdatedUser;

	

	//bi-directional many-to-one association to Address
	@JsonIgnore
	@OneToMany(mappedBy="countryMapping", fetch=FetchType.LAZY)
	private List<Address> addresses;

	//bi-directional many-to-one association to Customer
	@JsonIgnore
	@OneToMany(mappedBy="countryMapping")
	private List<Customer> customers;

	public CountryMapping() {
	}

	public Long getCountryId() {
		return this.countryId;
	}

	public void setCountryId(Long countryId) {
		this.countryId = countryId;
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Timestamp getCreatedTimestamp() {
		return this.createdTimestamp;
	}

	public void setCreatedTimestamp(Timestamp createdTimestamp) {
		this.createdTimestamp = createdTimestamp;
	}

	public String getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(String createdUser) {
		this.createdUser = createdUser;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Timestamp getLastUpdatedTimestamp() {
		return this.lastUpdatedTimestamp;
	}

	public void setLastUpdatedTimestamp(Timestamp lastUpdatedTimestamp) {
		this.lastUpdatedTimestamp = lastUpdatedTimestamp;
	}

	public String getLastUpdatedUser() {
		return this.lastUpdatedUser;
	}

	public void setLastUpdatedUser(String lastUpdatedUser) {
		this.lastUpdatedUser = lastUpdatedUser;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Address> getAddresses() {
		return this.addresses;
	}

	public void setAddresses(List<Address> addresses) {
		this.addresses = addresses;
	}

	public Address addAddress(Address address) {
		getAddresses().add(address);
		address.setCountryMapping(this);

		return address;
	}

	public Address removeAddress(Address address) {
		getAddresses().remove(address);
		address.setCountryMapping(null);

		return address;
	}

	public List<Customer> getCustomers() {
		return this.customers;
	}

	public void setCustomers(List<Customer> customers) {
		this.customers = customers;
	}

	public Customer addCustomer(Customer customer) {
		getCustomers().add(customer);
		customer.setCountryMapping(this);

		return customer;
	}

	public Customer removeCustomer(Customer customer) {
		getCustomers().remove(customer);
		customer.setCountryMapping(null);

		return customer;
	}

}