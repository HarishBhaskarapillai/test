/**
 * 
 */
package com.tcs.dess.bean;

/**
 * @author PocCoe
 *
 */
public class StatusResponse {

	private String message;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
}
