package com.tcs.dess.bean;

import java.io.Serializable;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonFilter;
import com.fasterxml.jackson.annotation.JsonIgnore;

import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the payee database table.
 * 
 */
@Entity
@JsonFilter("BANKAPI")
@NamedQuery(name="Payee.findAll", query="SELECT p FROM Payee p")
public class Payee implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="payee_id")
	private Long payeeId;

	@Column(name="added_date")
	private Timestamp addedDate;

	@Column(name="created_timestamp")
	private Timestamp createdTimestamp;

	@Column(name="created_user")
	private String createdUser;

	@Column(name="last_updated_timestamp")
	private Timestamp lastUpdatedTimestamp;

	@Column(name="last_updated_user")
	private String lastUpdatedUser;

	@Column(name="payee_nickname")
	private String payeeNickname;

	//bi-directional many-to-one association to Account
	@ManyToOne
	@JoinColumn(name="to_account_id")
	private Account account;
	
	@JsonIgnore
	@Column(name="to_account_id",insertable=false,updatable=false)
	private Long accountId;

	/**
	 * @return the accountId
	 */
	public Long getAccountId() {
		return accountId;
	}

	/**
	 * @param accountId the accountId to set
	 */
	public void setAccountId(Long accountId) {
		this.accountId = accountId;
	}

	//bi-directional many-to-one association to Transfer
	@JsonIgnore
	@OneToMany(mappedBy="payee")
	private List<Transfer> transfers;

	//bi-directional many-to-one association to Customer

	@ManyToOne
	@JoinColumn(name="customer_id")
	private Customer customer;
	
	/**
	 * @return the customerId
	 */
	public Long getCustomerId() {
		return customerId;
	}

	/**
	 * @param customerId the customerId to set
	 */
	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	@Column(name="customer_id",insertable=false,updatable=false)
	private Long customerId;

	public Payee() {
	}

	public Long getPayeeId() {
		return this.payeeId;
	}

	public void setPayeeId(Long payeeId) {
		this.payeeId = payeeId;
	}

	public Timestamp getAddedDate() {
		return this.addedDate;
	}

	public void setAddedDate(Timestamp addedDate) {
		this.addedDate = addedDate;
	}

	public Timestamp getCreatedTimestamp() {
		return this.createdTimestamp;
	}

	public void setCreatedTimestamp(Timestamp createdTimestamp) {
		this.createdTimestamp = createdTimestamp;
	}

	public String getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(String createdUser) {
		this.createdUser = createdUser;
	}

	public Timestamp getLastUpdatedTimestamp() {
		return this.lastUpdatedTimestamp;
	}

	public void setLastUpdatedTimestamp(Timestamp lastUpdatedTimestamp) {
		this.lastUpdatedTimestamp = lastUpdatedTimestamp;
	}

	public String getLastUpdatedUser() {
		return this.lastUpdatedUser;
	}

	public void setLastUpdatedUser(String lastUpdatedUser) {
		this.lastUpdatedUser = lastUpdatedUser;
	}

	public String getPayeeNickname() {
		return this.payeeNickname;
	}

	public void setPayeeNickname(String payeeNickname) {
		this.payeeNickname = payeeNickname;
	}

	public Account getAccount() {
		return this.account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}

	public List<Transfer> getTransfers() {
		return this.transfers;
	}

	public void setTransfers(List<Transfer> transfers) {
		this.transfers = transfers;
	}

	public Transfer addTransfer(Transfer transfer) {
		getTransfers().add(transfer);
		transfer.setPayee(this);

		return transfer;
	}

	public Transfer removeTransfer(Transfer transfer) {
		getTransfers().remove(transfer);
		transfer.setPayee(null);

		return transfer;
	}

	public Customer getCustomer() {
		return this.customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

}