/**
 * 
 * DataProcessingService.java 
 *
 * @author TCS
 * @Version 1.0 - 2016
 * 
 * @Copyright 2016 Tata Consultancy 
 */
package com.tcs.dess.service;

import static com.tcs.dess.utils.Constants.ACCESS_DENIED_MSG;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.tcs.dess.bean.RequestVO;
import com.tcs.dess.bean.User;
import com.tcs.dess.bean.UserT;
import com.tcs.dess.data.repository.UserRepository;
import com.tcs.dess.enums.JobName;
import com.tcs.dess.enums.UserGroup;
import com.tcs.dess.exception.MobilityException;
import com.tcs.dess.utils.FileManager;
import com.tcs.dess.utils.PropertyUtil;

/**
 * This DataProcessingService class <description>
 * 
 */
@Service("dataProcessingService")
public class DataProcessingService {
	
	private static final Logger logger = LoggerFactory
			.getLogger(DataProcessingService.class);
	
	@Value("${fileserver.path}")
	private String fileServerPath;
	
	@Autowired
	private UserRepository userRepository;


	/**
	 * @param file
	 * @param user 
	 * @return
	 * @throws Exception 
	 */
	public RequestVO processUploadRequest(MultipartFile file, User user) throws Exception {
		
		logger.debug("Inside processUploadRequest");
		
		User admin = userRepository.findByUserId(user.getUserId());
		RequestVO requestVO = null;
		
//		if(admin.getUserGroup().equals(UserGroup.Admin.getGroupName())) {
//			
//			FileManager.saveFile(file, fileServerPath);
//			requestVO = new RequestVO();
//			requestVO.setFileName(file.getOriginalFilename());
//			requestVO.setFilePath(fileServerPath);
//			requestVO.setRequestType("Upload");
//			requestVO.setJobName(JobName.userUpload);
//		
//		} else {
//			throw new MobilityException(HttpStatus.FORBIDDEN,
//					PropertyUtil.getProperty(ACCESS_DENIED_MSG));
//		}
		
		
		logger.debug("Exiting processUploadRequest service:");
		
		return requestVO;
	}

}
