/**
 * 
 * MobilityStepListener.java 
 *
 * @author TCS
 * @Version 1.0 - 2016
 * 
 * @Copyright 2016 Tata Consultancy 
 */
package com.tcs.dess.listener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;

/**
 * This MobilityStepListener class i used to log the failure in each step in batch execution
 * 
 */
public class MobilityStepListener implements StepExecutionListener {

	private static final Logger logger = LoggerFactory
			.getLogger(MobilityStepListener.class);

	@Override
	public void beforeStep(StepExecution stepExecution) {

	}

	/* (non-Javadoc)
	 * @see org.springframework.batch.core.StepExecutionListener#afterStep(org.springframework.batch.core.StepExecution)
	 */
	@Override
	public ExitStatus afterStep(StepExecution stepExecution) {
		

		ExitStatus exitStatus = stepExecution.getExitStatus();

		if (!exitStatus.equals(ExitStatus.COMPLETED)) {
			for (Throwable e: stepExecution.getFailureExceptions()) {
				logger.error("Error sending e-mail message: {}", e.getMessage());
			}
		}
		
		logger.info(
				"Step - {} exit with the exit code - {}, exit status - {}",
				stepExecution.getStepName(), exitStatus.getExitCode(),
				exitStatus.getExitDescription());

		return exitStatus;
	}

}
