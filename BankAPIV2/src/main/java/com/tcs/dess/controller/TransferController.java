/**
 * 
 * AccountController.java 
 *
 * @author TCS
 * @Version 1.0 - 2016
 * 
 * @Copyright 2016 Tata Consultancy 
 */
package com.tcs.dess.controller;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.tcs.dess.bean.Account;
import com.tcs.dess.bean.Customer;
import com.tcs.dess.bean.CustomerAccountRel;
import com.tcs.dess.bean.Payee;
import com.tcs.dess.bean.StatusResponse;
import com.tcs.dess.bean.Transfer;
import com.tcs.dess.exception.MobilityException;
import com.tcs.dess.service.AccountService;
import com.tcs.dess.service.TransferService;
import com.tcs.dess.utils.Constants;
import com.tcs.dess.utils.ResponseConstructors;

/**
 * Controller to handle batch requests.
 * 
 */
/**
 * This AccountController class holds services for handling Account
 * 
 */
@RestController
@RequestMapping("/transfer") 
public class TransferController {

	private static final Logger logger = LoggerFactory 
			.getLogger(TransferController.class);

	@Autowired
	TransferService transferService;
	
		
	
	@RequestMapping(value="/payee",method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<StatusResponse> addPayee(@RequestBody Payee payee){
		
		
		String response = "";
			try {
				
				response = transferService.addPayee(payee);
				
			} catch (Exception e) {
				e.printStackTrace();
				logger.error(e.getMessage());
				throw new MobilityException(HttpStatus.INTERNAL_SERVER_ERROR,
						e.getMessage());
			}
			
			StatusResponse statusResponse=new StatusResponse();
			
			if(response.equals("Customer / Account Not found")){
				statusResponse.setMessage(response);
				return new ResponseEntity<StatusResponse>(statusResponse, HttpStatus.NOT_FOUND);
			}
			else{
				statusResponse.setMessage("Payee Id Added : "+response);
				return new ResponseEntity<StatusResponse>(statusResponse, HttpStatus.OK);
			}
	}
	
	
	@RequestMapping(value="/payee",method = RequestMethod.GET,produces = "application/json")
	public @ResponseBody String  getPayee(
			@RequestParam(value="customer_id", defaultValue = "") Long customerId,
			@RequestParam(value = "view", defaultValue = "all")String view,
			@RequestParam(value = "fields", defaultValue = "all")String fields)
			{
			List<Payee> listOfPayee = new ArrayList<Payee>();
			try {
				   
				listOfPayee = transferService.getPayee(customerId);
				fields = Constants.GET_PAYEE_FIELDS;
				return ResponseConstructors.filterJsonForFieldAndViews(fields,view, listOfPayee);
				
			} catch (Exception e) {
				//e.printStackTrace();
				logger.error(e.getMessage());
				throw new MobilityException(HttpStatus.INTERNAL_SERVER_ERROR,
						e.getMessage());
			}
			
			
			
		
	}
	
	@RequestMapping(value="/payee",method = RequestMethod.DELETE)
	public @ResponseBody ResponseEntity<StatusResponse> deletePayee(@RequestBody Payee payee){
		
		
			try {
				
				
			} catch (Exception e) {
				e.printStackTrace();
				logger.error(e.getMessage());
				throw new MobilityException(HttpStatus.INTERNAL_SERVER_ERROR,
						e.getMessage());
			}
			StatusResponse statusResponse=new StatusResponse();
			statusResponse.setMessage("Success");
			
			return new ResponseEntity<StatusResponse>(statusResponse, HttpStatus.OK);
		
	}
	
	
	@RequestMapping(method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<StatusResponse> postTransfer(@RequestBody Transfer transfer){
		
		String transferId = null;
		
			try {
				 transferId = transferService.addTransfer(transfer);
				
				
			} catch (Exception e) {
				e.printStackTrace();
				logger.error(e.getMessage());
				throw new MobilityException(HttpStatus.INTERNAL_SERVER_ERROR,
						e.getMessage());
			}
			StatusResponse statusResponse=new StatusResponse();
			
			if(!transferId.equals(Constants.TRANSACTION_NOT_ENOUGH_BALANCE)){
				statusResponse.setMessage("Transfer Id added : "+transferId);
				return new ResponseEntity<StatusResponse>(statusResponse, HttpStatus.OK);
			}
			else{
				statusResponse.setMessage(Constants.TRANSACTION_NOT_ENOUGH_BALANCE);
				return new ResponseEntity<StatusResponse>(statusResponse,HttpStatus.BAD_REQUEST);
			}
			
		
	}
	
	@RequestMapping(method = RequestMethod.GET,produces = "application/json")
	public @ResponseBody String getTransfers(
			@RequestParam(value="account_number", defaultValue = "") String accountNumber,
			@RequestParam(value="account_type", defaultValue = "") Long accountType,
			@RequestParam(value = "view", defaultValue = "all")String view,
			@RequestParam(value = "fields", defaultValue = "all")String fields)
			{
		
			try {
				
				List<Transfer> allTransfers = transferService.getTransfers(accountNumber,accountType);
				fields = Constants.GET_TRANSFER_FIELDS;
				return ResponseConstructors.filterJsonForFieldAndViews(fields,view, allTransfers);
				
			} catch (Exception e) {
				e.printStackTrace();
				logger.error(e.getMessage());
				throw new MobilityException(HttpStatus.INTERNAL_SERVER_ERROR,
						e.getMessage());
			}
			
			
			
		
	}
	
		
	
}
