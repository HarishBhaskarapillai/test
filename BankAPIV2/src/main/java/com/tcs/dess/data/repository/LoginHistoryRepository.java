/**
 * 
 * LoginHistoryRepository.java 
 *
 * @author TCS
 * @Version 1.0 - 2016
 * 
 * @Copyright 2016 Tata Consultancy 
 */
package com.tcs.dess.data.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.tcs.dess.bean.LoginHistoryT;

/**
 * This LoginHistoryRepository class holds the CRUD services of LoginHistoryT bean
 * 
 */
public interface LoginHistoryRepository extends CrudRepository<LoginHistoryT, Integer> {
	
	/**
	 * Method to find the last login details for the userId
	 * @param userId
	 * @return LoginHistoryT
	 */
	@Query(value="select * from login_history_t where user_id = ?1 order by login_datetime desc limit 1", nativeQuery=true)
	LoginHistoryT findLastLoginByUserId(Long userId);
	
	/**
	 * Method to find the login details for the userId and sessionId
	 * @param userId
	 * @param sessionId
	 * @return LoginHistoryT
	 */
	LoginHistoryT findByUserIdAndSessionId(Long userId, String sessionId);
	
}
