/**
 * 
 */
package com.tcs.dess.controller;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.session.SessionInformation;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.tcs.dess.bean.LoginHistoryT;
import com.tcs.dess.bean.StatusResponse;
import com.tcs.dess.bean.User;
import com.tcs.dess.exception.MobilityException;
import com.tcs.dess.service.UserService;
import com.tcs.dess.utils.SessionUtil;

/**
 * @author PocCoe
 *
 */
@RestController
@RequestMapping("/user")
public class UserController {
 
	private static final Logger logger = LoggerFactory
			.getLogger(UserController.class);
	
	@Autowired
	UserService userService;
	
	@Autowired
	private SessionRegistry sessionRegistry;
	
	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<StatusResponse> userLogin(HttpServletRequest httpServletRequest)
	{
		logger.info("Login Invoked");
		StatusResponse statusResponse=new StatusResponse();
		try
		{
			List<SessionInformation> activeSessions = null;
			activeSessions = new ArrayList<>();
			
			for (Object principal : sessionRegistry.getAllPrincipals()) {
				activeSessions.addAll(sessionRegistry.getAllSessions(principal,
						false));
			}
			User user = userService.findByUserId(SessionUtil
					.getCurrentUserDetails().getUserId());
			  if(user!=null){
				HttpSession session = httpServletRequest.getSession(false);
  			 if (session == null) {
					session = httpServletRequest.getSession();
				}
				
				logger.info("active session size:" + activeSessions.size());
				statusResponse.setMessage("Success");
				System.out.println(session.getId());
				LoginHistoryT loginHistory = new LoginHistoryT();
			    loginHistory.setUserId(user.getUserId());
				loginHistory.setSessionId(session.getId());
				if (!userService.addLoginHistory(loginHistory)) {
					throw new MobilityException(
							HttpStatus.INTERNAL_SERVER_ERROR,
							"Could not save Login History");
				}
				return new ResponseEntity<StatusResponse>(statusResponse,HttpStatus.OK);
			}
			else
			{
				statusResponse.setMessage("NOT FOUND");
				return new ResponseEntity<StatusResponse>(statusResponse,HttpStatus.NOT_FOUND);
			}
			
		}catch(Exception e)
		{
			e.printStackTrace();
			logger.error(e.getMessage());
			throw new MobilityException(HttpStatus.INTERNAL_SERVER_ERROR,
					e.getMessage());
		}
		
	}
	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<StatusResponse> userLogout(HttpServletRequest httpServletRequest)
	{
		logger.info("Logout Invoked");
		StatusResponse statusResponse=new StatusResponse();
		try
		{
			HttpSession session = httpServletRequest.getSession(false);
			if (session != null) {
				logger.info("Logging out User Session: {}", session.getId());
				session.invalidate();
				sessionRegistry.removeSessionInformation(session.getId());
				statusResponse.setMessage("Success");
				return new ResponseEntity<StatusResponse>(statusResponse,HttpStatus.OK);
			} else {
				throw new MobilityException(HttpStatus.NOT_FOUND,
						"No valid session to log out");
			}
		}
		catch(Exception e)
		{
			logger.error(e.getMessage());
			throw new MobilityException(HttpStatus.INTERNAL_SERVER_ERROR,
					e.getMessage());
		}
			
}
	@RequestMapping(value = "/register", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<StatusResponse> userRegister(@RequestBody User user)
	{
		logger.info("Register Invoked");
		StatusResponse statusResponse=new StatusResponse();
		try
		{	
			userService.save(user);
			statusResponse.setMessage("success");
			return new ResponseEntity<StatusResponse>(statusResponse,HttpStatus.OK);
		}
		catch(Exception e)
		{
			e.printStackTrace();
			logger.error(e.getMessage());
			throw new MobilityException(HttpStatus.INTERNAL_SERVER_ERROR,
					e.getMessage());
		}
	}
	
}