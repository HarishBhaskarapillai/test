package com.tcs.dess.bean;

import java.util.List;

public class MasterDataMapper
{
    private List<MappingTable> mappingTable;

    public List<MappingTable> getMappingTable ()
    {
        return mappingTable;
    }

    public void setMappingTable (List<MappingTable> mappingTable)
    {
        this.mappingTable = mappingTable;
    }

   }