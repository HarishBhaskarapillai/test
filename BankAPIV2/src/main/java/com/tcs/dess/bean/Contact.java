package com.tcs.dess.bean;

import java.io.Serializable;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonFilter;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import java.sql.Timestamp;


/**
 * The persistent class for the contact database table.
 * 
 */
@Entity
@JsonFilter("BANKAPI")
//@JsonIdentityInfo(generator=ObjectIdGenerators.IntSequenceGenerator.class, property="contactId")
@NamedQuery(name="Contact.findAll", query="SELECT c FROM Contact c")
public class Contact implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="contact_id")
	private Long contactId;

	@Column(name="contact_value")
	private String contactValue;

	//bi-directional many-to-one association to ContactTypeMapping
	@ManyToOne
	@JoinColumn(name="contact_type")
	private ContactTypeMapping contactTypeMapping;
	
	@Column(name="contact_type", insertable = false, updatable = false)
	private Long contactType;
	
	
	/**
	 * @return the contactType
	 */
	public Long getContactType() {
		return contactType;
	}

	/**
	 * @param contactType the contactType to set
	 */
	public void setContactType(Long contactType) {
		this.contactType = contactType;
	}

	@Column(name="created_timestamp")
	private Timestamp createdTimestamp;

	@Column(name="created_user")
	private String createdUser;

	@Column(name="last_updated_timestamp")
	private Timestamp lastUpdatedTimestamp;

	@Column(name="last_updated_user")
	private String lastUpdatedUser;


	//bi-directional many-to-one association to Customer

	@ManyToOne
	@JsonIgnore
	@JoinColumn(name="customer_id")	
	private Customer customer;

	public Contact() {
	}

	public Long getContactId() {
		return this.contactId;
	}

	public void setContactId(Long contactId) {
		this.contactId = contactId;
	}

	public String getContactValue() {
		return this.contactValue;
	}

	public void setContactValue(String contactValue) {
		this.contactValue = contactValue;
	}

	public Timestamp getCreatedTimestamp() {
		return this.createdTimestamp;
	}

	public void setCreatedTimestamp(Timestamp createdTimestamp) {
		this.createdTimestamp = createdTimestamp;
	}

	public String getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(String createdUser) {
		this.createdUser = createdUser;
	}

	public Timestamp getLastUpdatedTimestamp() {
		return this.lastUpdatedTimestamp;
	}

	public void setLastUpdatedTimestamp(Timestamp lastUpdatedTimestamp) {
		this.lastUpdatedTimestamp = lastUpdatedTimestamp;
	}

	public String getLastUpdatedUser() {
		return this.lastUpdatedUser;
	}

	public void setLastUpdatedUser(String lastUpdatedUser) {
		this.lastUpdatedUser = lastUpdatedUser;
	}

	public ContactTypeMapping getContactTypeMapping() {
		return this.contactTypeMapping;
	}

	public void setContactTypeMapping(ContactTypeMapping contactTypeMapping) {
		this.contactTypeMapping = contactTypeMapping;
	}

	public Customer getCustomer() {
		return this.customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

}