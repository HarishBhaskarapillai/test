/**
 * 
 * Constants.java 
 *
 * @author TCS
 * @Version 1.0 - 2016
 * 
 * @Copyright 2016 Tata Consultancy 
 */
package com.tcs.dess.utils;


/**
 * This Constants class holds all the constants
 * 
 */
public class Constants {

	public static final String FILTER = "BankAPI";
	public static final String TIME_OUT = "TIME_OUT";
	public static final String NO = "NO";
	public static final String YES = "YES";
	public static final String Y = "Y";
	public static final String N = "N";
	public static final String FIELD = "F";
	public static final String COLLECTION = "C";
	public static final String ID_FIELD = "I";
	public static final String SYSTEM_USER = "System";
	public static final String SYSTEM_ADMIN = "System Admin";
	public static final String MIME = "MIME";
	public static final String UTF8 = "UTF-8";
	public static final String LOGIN_APP_VERSION = "APP_VERSION";
	public static final String GLOBAL = "GLOBAL_CUSTOMERS";
	public static final String SINGLE_QUOTE = "'";
	public static final String DOUBLE_SINGLE_QUOTE = "''";
	public static final String COMMA = ",";
	public static final String RIGHT_PARANTHESIS = ")";
	public static final String LEFT_PARANTHESIS = "(";
	public static final String AND_CLAUSE = " and ";
	public static final String OR_CLAUSE = " or ";
	public static final int ONE_DAY_IN_MILLIS = 86400000;
	public static final String USD = "USD";
	public static final String SPACE = "";
	public static final String VALIDATOR_SHEET_NAME="Validate";
	public static final int OPPORTUNITY_DESC_MAX_SIZE = 1000;
	public static final int ENGAGEMENT_DURATION_MAX_SIZE = 15;
	public static final int NOTEST_MAX_SIZE = 1000;
	public static final int CORE_ATTRIBUTES_MAX_VALUE = 300;
	public static final String ROWNUMBER = "Row Number";
	public static final String SHEETNAME = "Sheet Name";
	public static final String ACTION_ADD = "Add"; 
	
	public static final String APPLICATION_PROPERTIES_FILENAME = "application";
	
	public static final String DATE_TYPE = "dateType";
	public static final String DATE = "date";
	public static final String FILE_DIR_SEPERATOR = "/";
	public static final String FILE_PATH = "FILE_PATH";
	public static final String USER_ID = "USER_ID";
	public static final String NEXT_STEP = "NEXT_STEP";
	public static final String REQUEST_ID = "REQUEST_ID";
	public static final String REQUEST = "REQUEST";
	
	public static final String DOWNLOAD = "download";
	public static final String UPLOAD = "upload";
	
	public static final String ENVIRONMENT_NAME = "environment.name";
	public static final String BAD_CREDENTIALS_MSG = "AbstractUserDetailsAuthenticationProvider.badCredentials";
	public static final String ACCESS_DENIED_MSG = "access.denied";
	
	
	//BankAPI Specific Constants 
	public static final String FIND_API_VIEW_GENERIC="customers,customerId,firstName,lastName,middleName,prefix,suffix,dob,gender";
	public static final String FIND_API_VIEW_IDENTIFICATION="identifications,identificationId,identificationValue,identificationTypeMapping,type,description";
	public static final String FIND_API_VIEW_CONTACT="contacts,contactId,contactValue,contactTypeMapping,type,description";

	public static final String TRANSACTION_API_VIEW_GENERIC="transactionId,transactionType,amount,description,transactionDate,account,accountId,accountNumber,accountTypeMapping,accountTypeId,type";

	public static final String FIND_API_VIEW_ACCOUNT_BASIC="account,accountId,accountNumber,accountType";
	public static final String FIND_API_VIEW_ACCOUNT_EXTENDED="account,accountId,accountNumber,accountType,productCode,accountBalance,accountStatusMapping,accountStatusId,status";
	public static final String FIND_API_VIEW_ACCOUNT_OTHER="account,accountId,accountNumber,productCode";
	public static final String FIND_API_VIEW_ACCOUNT_BALANCE="account,accountId,accountNumber,accountType,accountBalance";
	public static final Long ACCOUNT_STATUS_CLOSED_ID = new Long(3);
	public static final String TRANSACTION_TYPE_CREDIT = "C";
	public static final String TRANSACTION_TYPE_DEBIT = "D"; 
	public static final String TRANSACTION_NOT_ENOUGH_BALANCE = "Not Enough balance";
	
	public static final String GET_PAYEE_FIELDS = "payee,payeeId,payeeNickname,account,accountId,accountNumber,accountTypeMapping,type,customer,customerId";
	
	public static final String GET_TRANSFER_FIELDS = "transfer,transferId,transferAmount,transferDate,transferType,account,accountId,accountNumber,accountTypeMapping,type,"
			+ "payee,payeeId,payeeNickname,account,accountId,accountNumber,accountTypeMapping,type";
}