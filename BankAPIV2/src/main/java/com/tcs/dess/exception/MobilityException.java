/**
 * 
 * MobilityException.java 
 *
 * @author TCS
 * @Version 1.0 - 2016
 * 
 * @Copyright 2016 Tata Consultancy 
 */
package com.tcs.dess.exception;

import org.springframework.http.HttpStatus;

/**
 * This MobilityException class is the praent class for the exception framework in mobility
 * 
 */
public class MobilityException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String message = null;
	private HttpStatus httpStatus = null;

	public MobilityException() {
		super();
	}

	public MobilityException(HttpStatus httpStatus, String message) {
		super(message);
		this.message = message;
		this.httpStatus = httpStatus;
	}

	@Override
	public String toString() {
		return message;
	}

	@Override
	public String getMessage() {
		return message;
	}

	public HttpStatus getHttpStatus() {
		return httpStatus;
	}
}