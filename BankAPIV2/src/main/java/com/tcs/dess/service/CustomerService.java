/**
 * 
 * CustomerService.java 
 *
 * @author TCS
 * @Version 1.0 - 2016
 * 
 * @Copyright 2016 Tata Consultancy 
 */
package com.tcs.dess.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tcs.dess.bean.Address;
import com.tcs.dess.bean.Contact;
import com.tcs.dess.bean.Customer;
import com.tcs.dess.bean.CustomerAddressRel;
import com.tcs.dess.bean.Identification;
import com.tcs.dess.bean.LoginHistoryT;
import com.tcs.dess.bean.UserT;
import com.tcs.dess.data.repository.AddressRepository;
import com.tcs.dess.data.repository.ContactRepository;
import com.tcs.dess.data.repository.CustomerAddressRelRepository;
import com.tcs.dess.data.repository.CustomerRepository;
import com.tcs.dess.data.repository.IdentificationRepository;
import com.tcs.dess.data.repository.LoginHistoryRepository;
import com.tcs.dess.data.repository.UserRepository;

/**
 * 
 * This service handles functionalities related to Customer
 *
 */
@Service
@Transactional
public class CustomerService {

	private static final Logger logger = LoggerFactory
			.getLogger(CustomerService.class);
	
		
	@Autowired
	private CustomerRepository customerRepository;
	
	@Autowired
	private ContactRepository contactRepository;
	
	@Autowired
	private IdentificationRepository identificationRepository;
	
	@Autowired
	private AddressRepository addressRepository;
	
	@Autowired
	private CustomerAddressRelRepository customerAddressRelRepository;
	
	/**
	 * This method is used to retrieve user details based on userId
	 * @param userId
	 * @return
	 * @throws Exception
	 */
	public String save(Customer customer) throws Exception {
		logger.info("FirstName  :"+customer.getFirstName());
		logger.info("Contact Details : "+customer.getContacts().get(0).getContactValue());
		Customer savedCustomer = customerRepository.save(customer);		
//		List<Contact> contacts = customer.getContacts();
//		List<Identification> identifications = customer.getIdentifications();
		List<CustomerAddressRel> customerAddressRels= customer.getCustomerAddressRels();
		
		if(customerAddressRels != null){
			for(int i=0;i<customerAddressRels.size();i++){
				CustomerAddressRel customerAddressRel = customer.getCustomerAddressRels().get(i);
				Address address = customerAddressRel.getAddress();
				address = addressRepository.save(address);
				customerAddressRel.setCustomer(savedCustomer);
				customerAddressRel.setAddress(address);
				customerAddressRelRepository.save(customerAddressRel);
			}
		}
		
				
		return savedCustomer.getCustomerId()+"";
	}
	
	public String update(Customer customer) throws Exception {
		
		//Save customer
		
		Customer savedCustomer = customerRepository.save(customer);		
		
		
		return customer.getCustomerId()+"";
	}

	public Customer getCustomerProfile(Long customerId) throws Exception{
		
		logger.info("CustomerId in Service: "+customerId);
		Customer customer = customerRepository.findOne(customerId);
		logger.info(customer.getFirstName());
		
		return customer;
		
	}
	
	
	public List<Customer> findByFirstNameAndLastName(String firstName, String lastName) throws Exception {
		return customerRepository.findByFirstNameAndLastName(firstName, lastName);
	}
	
	public List<Customer> findByIdentification(Long identificationType, String identificationValue) throws Exception {
		List<Identification> identifications = identificationRepository.
				findByIdentificationValueAndIdentificationType(identificationValue,identificationType);
		
		List<Customer> customers = new ArrayList<Customer>();
		
		HashSet<Identification> uniqueRecords = new HashSet<Identification>(identifications);
		
		logger.info("uniqueRecords Size  :  "+uniqueRecords.size());
		
		 Iterator iterator = uniqueRecords.iterator(); 
		 
			
		while(iterator.hasNext()){
			Identification id = (Identification) iterator.next();
			Long customerId = id.getCustomer().getCustomerId();
			logger.info("Inside While Loop  " + customerId);
			customers.add(customerRepository.findOne(customerId));
		}
		
		return customers;
	}
	
	public List<Customer> findByContact(Long contactType, String contactValue) throws Exception {
		List<Contact> contacts = contactRepository.
				findByContactTypeAndContactValue(contactType,contactValue);
		
		List<Customer> customers = new ArrayList<Customer>();
		
		HashSet<Contact> uniqueRecords = new HashSet<Contact>(contacts);
		
		logger.info("uniqueRecords Size  :  "+uniqueRecords.size());
		
		 Iterator iterator = uniqueRecords.iterator(); 
		 
			
		while(iterator.hasNext()){
			Contact con = (Contact) iterator.next();
			Long customerId =con.getCustomer().getCustomerId();
			logger.info("Inside While Loop  " + customerId);
			customers.add(customerRepository.findOne(customerId));
		}
		
		return customers;
	}
}