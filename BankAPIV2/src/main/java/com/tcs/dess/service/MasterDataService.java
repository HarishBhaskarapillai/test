/**
 * 
 * AccountService.java 
 *
 * @author TCS
 * @Version 1.0 - 2016
 * 
 * @Copyright 2016 Tata Consultancy 
 */
package com.tcs.dess.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tcs.dess.bean.AccountRelationTypeMapping;
import com.tcs.dess.bean.AccountStatusMapping;
import com.tcs.dess.bean.AccountTypeMapping;
import com.tcs.dess.bean.AddressTypeMapping;
import com.tcs.dess.bean.BranchCodeMapping;
import com.tcs.dess.bean.CodeValue;
import com.tcs.dess.bean.ContactTypeMapping;
import com.tcs.dess.bean.CountryMapping;
import com.tcs.dess.bean.IdentificationTypeMapping;
import com.tcs.dess.bean.MappingTable;
import com.tcs.dess.bean.MasterDataMapper;
import com.tcs.dess.bean.StateMapping;
import com.tcs.dess.data.repository.AccountRelationTypeMappingRepository;
import com.tcs.dess.data.repository.AccountStatusMappingRepository;
import com.tcs.dess.data.repository.AccountTypeMappingRepository;
import com.tcs.dess.data.repository.AddressTypeMappingRepository;
import com.tcs.dess.data.repository.BranchCodeMappingRepository;
import com.tcs.dess.data.repository.ContactTypeMappingRepository;
import com.tcs.dess.data.repository.CountryMappingRepository;
import com.tcs.dess.data.repository.IdentificationTypeMappingRepository;
import com.tcs.dess.data.repository.StateMappingRepository;

/**
 * 
 * This service handles functionalities related to Account
 *
 */
@Service
public class MasterDataService {

	private static final Logger logger = LoggerFactory
			.getLogger(MasterDataService.class);
	
		
	@Autowired
	private AccountRelationTypeMappingRepository accountRelationTypeMappingRepository;
	
	@Autowired
	private AccountStatusMappingRepository accountStatusMappingRepository;


	@Autowired
	private AccountTypeMappingRepository accountTypeMappingRepository;
	
	@Autowired
	private AddressTypeMappingRepository addressTypeMappingRepository;
	
	@Autowired
	private BranchCodeMappingRepository branchCodeMappingRepository;
	
	@Autowired
	private ContactTypeMappingRepository contactTypeMappingRepository;	
	
	
	@Autowired
	private CountryMappingRepository countryMappingRepository;		
	
	
	@Autowired
	private IdentificationTypeMappingRepository identificationTypeMappingRepository;		
	
	@Autowired
	private StateMappingRepository stateMappingRepository;	
	
	
	/**
	 * 
	 */
	public MasterDataMapper fetchMasterData() {
		
		
		
		MasterDataMapper masterDataMapper = new MasterDataMapper();
		
		List<MappingTable> mappingTables = new ArrayList();
		
		
		
		Iterator accountRelationTypeMapping = accountRelationTypeMappingRepository.findAll().iterator();
		MappingTable mappingTable = new MappingTable();
		mappingTable.setName("AccountRelationTypeMapping");
		List<CodeValue> codeValues = new ArrayList();
		int i=0;
		while(accountRelationTypeMapping.hasNext()){	
			CodeValue codeValue = new CodeValue();
			AccountRelationTypeMapping accountRelationType = (AccountRelationTypeMapping) accountRelationTypeMapping.next();
			codeValue.setId(accountRelationType.getAccountRelationTypeId());
			codeValue.setCode(accountRelationType.getStatus());	
			codeValues.add(codeValue);
			i++;
		}
		mappingTable.setCodeValue(codeValues);
		mappingTables.add(mappingTable);
		
		
		Iterator accountStatusMapping = accountStatusMappingRepository.findAll().iterator();
		mappingTable = new MappingTable();
		mappingTable.setName("AccountStatusMapping");
		codeValues = new ArrayList();
		i=0;
		while(accountStatusMapping.hasNext()){	
			CodeValue codeValue = new CodeValue();
			AccountStatusMapping accountStatusMap = (AccountStatusMapping) accountStatusMapping.next();
			codeValue.setId(accountStatusMap.getAccountStatusId());
			codeValue.setCode(accountStatusMap.getStatus());	
			codeValues.add(codeValue);
			i++;
		}
		mappingTable.setCodeValue(codeValues);
		mappingTables.add(mappingTable);
		
		
		
		Iterator accountTypeMapping = accountTypeMappingRepository.findAll().iterator();
		mappingTable = new MappingTable();
		mappingTable.setName("AccountTypeMapping");
		codeValues = new ArrayList();
		i=0;
		while(accountTypeMapping.hasNext()){	
			CodeValue codeValue = new CodeValue();
			AccountTypeMapping accountTypeMap = (AccountTypeMapping) accountTypeMapping.next();
			codeValue.setId(accountTypeMap.getAccountTypeId());
			codeValue.setCode(accountTypeMap.getType());	
			codeValues.add(codeValue);
			i++;
		}
		mappingTable.setCodeValue(codeValues);
		mappingTables.add(mappingTable);
		
		
		
		
		Iterator addressTypeMapping = addressTypeMappingRepository.findAll().iterator();
		mappingTable = new MappingTable();
		mappingTable.setName("AddressTypeMapping");
		codeValues = new ArrayList();
		i=0;
		while(addressTypeMapping.hasNext()){	
			CodeValue codeValue = new CodeValue();
			AddressTypeMapping addressTypeMap = (AddressTypeMapping) addressTypeMapping.next();
			codeValue.setId(addressTypeMap.getAddressTypeId());
			codeValue.setCode(addressTypeMap.getType());	
			codeValues.add(codeValue);
			i++;
		}
		mappingTable.setCodeValue(codeValues);
		mappingTables.add(mappingTable);
		
		
		
		
		Iterator branchCodeMapping = branchCodeMappingRepository.findAll().iterator();
		mappingTable = new MappingTable();
		mappingTable.setName("BranchCodeMapping");
		codeValues = new ArrayList();
		i=0;
		while(branchCodeMapping.hasNext()){	
			CodeValue codeValue = new CodeValue();
			BranchCodeMapping branchCodeMap = (BranchCodeMapping) branchCodeMapping.next();
			codeValue.setId(branchCodeMap.getBranchCodeId());
			codeValue.setCode(branchCodeMap.getBranchCode());	
			codeValues.add(codeValue);
			i++;
		}
		mappingTable.setCodeValue(codeValues);
		mappingTables.add(mappingTable);
		
		
		Iterator contactTypeMapping = contactTypeMappingRepository.findAll().iterator();
		mappingTable = new MappingTable();
		mappingTable.setName("ContactTypeMapping");
		codeValues = new ArrayList();
		i=0;
		while(contactTypeMapping.hasNext()){	
			CodeValue codeValue = new CodeValue();
			ContactTypeMapping contactTypeMap = (ContactTypeMapping) contactTypeMapping.next();
			codeValue.setId(contactTypeMap.getContactTypeId());
			codeValue.setCode(contactTypeMap.getType());	
			codeValues.add(codeValue);
			i++;
		}
		mappingTable.setCodeValue(codeValues);
		mappingTables.add(mappingTable);
		
		
		Iterator countryMapping = countryMappingRepository.findAll().iterator();
		mappingTable = new MappingTable();
		mappingTable.setName("CountryMapping");
		codeValues = new ArrayList();
		i=0;
		while(countryMapping.hasNext()){	
			CodeValue codeValue = new CodeValue();
			CountryMapping countryMap = (CountryMapping) countryMapping.next();
			codeValue.setId(countryMap.getCountryId());
			codeValue.setCode(countryMap.getName());	
			codeValues.add(codeValue);
			i++;
		}
		mappingTable.setCodeValue(codeValues);
		mappingTables.add(mappingTable);
		
		
		Iterator identificationTypeMapping = identificationTypeMappingRepository.findAll().iterator();
		mappingTable = new MappingTable();
		mappingTable.setName("IdentificationTypeMapping");
		codeValues = new ArrayList();
		i=0;
		while(identificationTypeMapping.hasNext()){	
			CodeValue codeValue = new CodeValue();
			IdentificationTypeMapping identificationTypeMap = (IdentificationTypeMapping) identificationTypeMapping.next();
			codeValue.setId(identificationTypeMap.getIdentificationTypeId());
			codeValue.setCode(identificationTypeMap.getType());	
			codeValues.add(codeValue);
			i++;
		}
		mappingTable.setCodeValue(codeValues);
		mappingTables.add(mappingTable);
		
		
		Iterator stateMapping = stateMappingRepository.findAll().iterator();
		mappingTable = new MappingTable();
		mappingTable.setName("StateMapping");
		codeValues = new ArrayList();
		i=0;
		while(stateMapping.hasNext()){	
			CodeValue codeValue = new CodeValue();
			StateMapping stateMap = (StateMapping) stateMapping.next();
			codeValue.setId(stateMap.getStateId());
			codeValue.setCode(stateMap.getName());	
			codeValues.add(codeValue);
			i++;
		}
		mappingTable.setCodeValue(codeValues);
		mappingTables.add(mappingTable);
		
		
		masterDataMapper.setMappingTable(mappingTables);
		
		return masterDataMapper;
	}
	
	
	
}