/**
 * 
 * LoginHistoryRepository.java 
 *
 * @author TCS
 * @Version 1.0 - 2016
 * 
 * @Copyright 2016 Tata Consultancy 
 */
package com.tcs.dess.data.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.tcs.dess.bean.Account;
import com.tcs.dess.bean.Address;
import com.tcs.dess.bean.Contact;
import com.tcs.dess.bean.Customer;
import com.tcs.dess.bean.Transaction;
import com.tcs.dess.bean.UserT;

/**
 * This UserRepository class holds the CRUD services of UserT bean
 * 
 */
@Repository
public interface TransactionRepository extends CrudRepository<Transaction, Long> {

	/**
	 * @param accountNumber
	 * @param accountType
	 */
	List<Transaction> findByAccountIdOrderByTransactionDateDesc(Long accountId);
	List<Transaction> findByTransactionDateBetweenAndAccountIdOrderByTransactionDateDesc(Date startDate, Date endDate, Long accountId);
	/**
	 * @param account
	 * @return
	 */
		
	
}
