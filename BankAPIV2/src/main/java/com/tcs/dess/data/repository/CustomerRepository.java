/**
 * 
 * LoginHistoryRepository.java 
 *
 * @author TCS
 * @Version 1.0 - 2016
 * 
 * @Copyright 2016 Tata Consultancy 
 */
package com.tcs.dess.data.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.tcs.dess.bean.Customer;
import com.tcs.dess.bean.UserT;

/**
 * This UserRepository class holds the CRUD services of UserT bean
 * 
 */
@Repository
public interface CustomerRepository extends CrudRepository<Customer, Long> {
	
	List<Customer> findByFirstNameAndLastName(String firstName, String lastName);
	
}
