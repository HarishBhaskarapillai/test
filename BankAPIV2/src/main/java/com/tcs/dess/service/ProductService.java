/**
 * 
 * ProductService.java 
 *
 * @author TCS
 * @Version 1.0 - 2016
 * 
 * @Copyright 2016 Tata Consultancy 
 */
package com.tcs.dess.service;

import java.sql.Timestamp;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tcs.dess.bean.Customer;
import com.tcs.dess.bean.LoginHistoryT;
import com.tcs.dess.bean.UserT;
import com.tcs.dess.data.repository.CustomerRepository;
import com.tcs.dess.data.repository.LoginHistoryRepository;
import com.tcs.dess.data.repository.UserRepository;

/**
 * 
 * This service handles functionalities related to Products
 *
 */
@Service
public class ProductService {

	private static final Logger logger = LoggerFactory
			.getLogger(ProductService.class);
	
		
	@Autowired
	private CustomerRepository customerRepository;
	
	/**
	 * This method is used to retrieve user details based on userId
	 * @param userId
	 * @return
	 * @throws Exception
	 */
	
}