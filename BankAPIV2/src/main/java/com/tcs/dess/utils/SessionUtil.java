/**
 * 
 * SessionUtil.java 
 *
 * @author TCS
 * @Version 1.0 - 2016
 * 
 * @Copyright 2016 Tata Consultancy 
 */
package com.tcs.dess.utils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import com.tcs.dess.bean.User;
import com.tcs.dess.bean.UserT;
import com.tcs.dess.controller.UserRepositoryUserDetailsService.UserRepositoryUserDetails;
import com.tcs.dess.exception.MobilityException;

/**
 * This SessionUtil class provides features to handle session details
 * 
 */
public class SessionUtil {

	// This method returns the current session authenticated user details
	public static User getCurrentUserDetails() {
		Authentication a = SecurityContextHolder.getContext()
				.getAuthentication();
		if(a instanceof AnonymousAuthenticationToken){
			System.out.println("into anon");
			return null;
		}
		return ((UserRepositoryUserDetails) a.getPrincipal());
	}

	// This method verifies if the user id passed in the URI parameter 
	// is same as current session authenticated user
	public static boolean isCurrentAuthenticatedUser(Long userId) {
		if (getCurrentUserDetails().getUserId().equals(userId))
			return true;
		else
			return false;
	}

	/**
	 * Returns a copy of the object, or null if the object cannot
	 * be serialized.
	 */
	public static Object copy(Object originalObject) throws Exception {
		Object copyObject = null;
		try {
			// Write the object to a byte array
			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			ObjectOutputStream oos = new ObjectOutputStream(bos);
			oos.writeObject(originalObject);
			oos.flush();
			oos.close();

			// Make an input stream from the byte array and read a copy of the object
			ObjectInputStream ois = new ObjectInputStream(
					new ByteArrayInputStream(bos.toByteArray()));
			copyObject = ois.readObject();
		} catch(IOException ioe) {
			throw new MobilityException(HttpStatus.INTERNAL_SERVER_ERROR, 
					"Error occurred while cloning the object:" + ioe.getMessage());
		} catch(ClassNotFoundException cnfe) {
			throw new MobilityException(HttpStatus.INTERNAL_SERVER_ERROR, 
					"Error occurred while cloning the object:" + cnfe.getMessage());
		}
		return copyObject;
	}
	
}
