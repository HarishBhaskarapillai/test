/**
 * 
 * GlobalExceptionController.java 
 *
 * @author TCS
 * @Version 1.0 - 2016
 * 
 * @Copyright 2016 Tata Consultancy 
 */
package com.tcs.dess.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.tcs.dess.enums.Status;
import com.tcs.dess.exception.MobilityException;
import com.tcs.dess.utils.SessionUtil;

/**
 * Controller class to handle application exceptions.
 * 
 */
@ControllerAdvice
public class GlobalExceptionController {

	private static final Logger logger = LoggerFactory.getLogger(GlobalExceptionController.class);

	/**
	 * Exception handler method for MobilityException
	 * 
	 * @param MobilityException
	 * @return ResponseEntity
	 */
	@ExceptionHandler(MobilityException.class)
	public ResponseEntity<Status> handleMobilityException(MobilityException de) {
		if (SessionUtil.getCurrentUserDetails() != null) 
			MDC.put("userId", SessionUtil.getCurrentUserDetails().getUserId().toString());
		logger.error("MobilityException: Status-" + de.getHttpStatus()
				+ " Message-" + de.getMessage());
		Status status = new Status();
		status.setStatus(Status.FAILED, de.getMessage());
		return new ResponseEntity<Status> (status, de.getHttpStatus());
	}

	/**
	 * Exception handler method for all Exceptions
	 * 
	 * @param Exception
	 * @return ResponseEntity
	 */
	@ExceptionHandler(Exception.class)
	public ResponseEntity<Status> handleAllException(Exception e) {
		if (SessionUtil.getCurrentUserDetails() != null) 
			MDC.put("userId", SessionUtil.getCurrentUserDetails().getUserId().toString());
		logger.error("Exception: Status-" + HttpStatus.INTERNAL_SERVER_ERROR
				+ " Message-" + e.getMessage());
		Status status = new Status();
		status.setStatus(Status.FAILED, e.getMessage());
		return new ResponseEntity<Status>(status, HttpStatus.INTERNAL_SERVER_ERROR);
	}

}
