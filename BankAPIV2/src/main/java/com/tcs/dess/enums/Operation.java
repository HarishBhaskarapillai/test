/**
 * 
 * JobName.java 
 *
 * @author TCS
 * @Version 1.0 - 2016
 * 
 * @Copyright 2016 Tata Consultancy 
 */
 package com.tcs.dess.enums;

/**
 * This Operation enum contains the operation constants supported
 * 
 */
public enum Operation {
	
	ADD, UPDATE, DELETE

}
