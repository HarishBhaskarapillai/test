/**
 * 
 * LoginHistoryRepository.java 
 *
 * @author TCS
 * @Version 1.0 - 2016
 * 
 * @Copyright 2016 Tata Consultancy 
 */
package com.tcs.dess.data.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.tcs.dess.bean.Account;
import com.tcs.dess.bean.Payee;


/**
 * This UserRepository class holds the CRUD services of UserT bean
 * 
 */
@Repository
public interface PayeeRepository extends CrudRepository<Payee, Long> {


	/**
	 * @param accountId
	 * @return
	 */
	List<Payee> findByCustomerId(Long customerId);

	/**
	 * @param accountId
	 * @return
	 */
	List<Payee> findByAccountId(Long accountId);  

	
	
}
