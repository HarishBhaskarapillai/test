/**
 * 
 * ResponseConstructors.java 
 *
 * @author TCS
 * @Version 1.0 - 2016
 * 
 * @Copyright 2016 Tata Consultancy 
 */
package com.tcs.dess.utils;

import java.util.HashSet;
import java.util.Set;
import java.util.StringTokenizer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.ser.FilterProvider;
import com.fasterxml.jackson.databind.ser.impl.SimpleBeanPropertyFilter;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;
import com.tcs.dess.mapper.ViewFieldsMapper;

/**
 * This ResponseConstructors class provide features to construct response for the service
 * 
 */
public class ResponseConstructors {

	private static final Logger logger = LoggerFactory.getLogger(ResponseConstructors.class);

	public static String filterJsonForFieldAndViews(String fields, String view,
			Object object) throws Exception {
		StringBuffer viewFields = null;
		if (!view.equals("")) {
			viewFields = new StringBuffer();
			StringTokenizer st = new StringTokenizer(view, ",");
			while (st.hasMoreTokens()) {
				String v_fields = ViewFieldsMapper.getFields(st.nextToken());
				if (v_fields != null)
					viewFields.append(v_fields);
			}
		}
		if ((viewFields != null) && (viewFields.length() > 0)) {
			if (fields.equalsIgnoreCase("all")) {
				fields = viewFields.toString();
			} else { 
				fields = fields.concat("," + viewFields.toString());
			}
		}
		
		logger.info("View : "+view+"   Fields : "+fields);
		return filterJsonForFields(fields, object);

	}

	public static String filterJsonForFields(String fields, Object object) throws Exception {
		ObjectMapper mapper = new ObjectMapper();
		//For Jackson to understand Hibernate datatypes (e.g .Lazy loading)
		//mapper.registerModule(new Hibernate4Module());
		mapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
		
		if (fields.equals("all")) {
			try {
				FilterProvider filters = new SimpleFilterProvider().addFilter(
						"BANKAPI",
						SimpleBeanPropertyFilter.serializeAllExcept(""));
				return mapper.writer(filters).writeValueAsString(object);
			} catch (JsonProcessingException e) {
				return e.getMessage();
			}
		} else {
			logger.info("fields inside Response Constructor : "+fields);
			StringTokenizer st = new StringTokenizer(fields, ",");
			Set<String> filterProperties = new HashSet<String>();
			String objectId = null;
			while (st.hasMoreTokens()) {
				String token = st.nextToken();
				filterProperties.add(token);
				//Add object id to support JsonIdentityInfo object reference
				objectId = BeanObjectIdMapper.getObjectId(token); 
				if (objectId != null) {
					logger.info("Adding ObjectId: {}", objectId);
					filterProperties.add(objectId);
				}
			}

			FilterProvider filters = new SimpleFilterProvider().addFilter(
					"BANKAPI", SimpleBeanPropertyFilter
							.filterOutAllExcept(filterProperties));
			try {
				
				String response = mapper.writer(filters).writeValueAsString(object);
				logger.info("Response::::::::::::::::::: " + response);
				return response;
			} catch (Exception e) {
				e.printStackTrace();
				return e.getMessage();
			}

		}
	}
	
}
