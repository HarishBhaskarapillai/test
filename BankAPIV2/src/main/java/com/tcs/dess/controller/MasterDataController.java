/**
 * 
 * AccountController.java 
 *
 * @author TCS
 * @Version 1.0 - 2016
 * 
 * @Copyright 2016 Tata Consultancy 
 */
package com.tcs.dess.controller;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.tcs.dess.bean.Customer;
import com.tcs.dess.bean.MasterDataMapper;
import com.tcs.dess.bean.RequestVO;
import com.tcs.dess.bean.UserT;
import com.tcs.dess.enums.Status;
import com.tcs.dess.exception.MobilityException;
import com.tcs.dess.service.AccountService;
import com.tcs.dess.service.CustomerService;
import com.tcs.dess.service.MasterDataService;
import com.tcs.dess.utils.ResponseConstructors;
import com.tcs.dess.utils.SessionUtil;

/**
 * This AccountController class holds services for handling Account
 * 
 */
@RestController
@RequestMapping("/master")
public class MasterDataController {

	private static final Logger logger = LoggerFactory
			.getLogger(MasterDataController.class);

	@Autowired
	MasterDataService masterDataService;
	
	
	@RequestMapping(method = RequestMethod.GET,produces = "application/json")
	public @ResponseBody String fetchMasterData(){
		try {
		MasterDataMapper masterDataMapper = masterDataService.fetchMasterData();

		return ResponseConstructors.filterJsonForFieldAndViews("all","all", masterDataMapper);
		} catch (Exception e) {
			//e.printStackTrace();
			logger.error(e.getMessage());
			throw new MobilityException(HttpStatus.INTERNAL_SERVER_ERROR,
					e.getMessage());
		}
	}
	

}
