/**
 * 
 * AccountService.java 
 *
 * @author TCS
 * @Version 1.0 - 2016
 * 
 * @Copyright 2016 Tata Consultancy 
 */
package com.tcs.dess.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tcs.dess.bean.Account;
import com.tcs.dess.bean.AccountRelationTypeMapping;
import com.tcs.dess.bean.AccountStatusMapping;
import com.tcs.dess.bean.Address;
import com.tcs.dess.bean.Customer;
import com.tcs.dess.bean.CustomerAccountRel;
import com.tcs.dess.bean.CustomerAddressRel;
import com.tcs.dess.data.repository.AccountRelationTypeMappingRepository;
import com.tcs.dess.data.repository.AccountRepository;
import com.tcs.dess.data.repository.AccountStatusMappingRepository;
import com.tcs.dess.data.repository.CustomerAccountRelRepository;
import com.tcs.dess.data.repository.CustomerRepository;
import com.tcs.dess.utils.Constants;

/**
 * 
 * This service handles functionalities related to Account
 *
 */
@Service
public class AccountService {

	@SuppressWarnings("unused")
	private static final Logger logger = LoggerFactory
			.getLogger(AccountService.class);
	
		
	
	@Autowired
	private AccountRepository accountRepository;
	
	@Autowired
	private CustomerRepository customerRepository;
	
	@Autowired
	CustomerAccountRelRepository customerAccountRelRepository;
	
	@Autowired
	AccountRelationTypeMappingRepository accountRelationTypeMappingRepository;
	
	
	@Autowired
	AccountStatusMappingRepository accountStatusMappingRepository;
	
	/**
	 * This method is used to retrieve user details based on userId
	 * @param userId
	 * @return
	 * @throws Exception
	 */
	
	public String save(CustomerAccountRel customerAccountRel) throws Exception {
		
		//AccountTypeMapping accountTypeMapping = accountTypeMappingRepository.save(account.getAccountTypeMapping());
		//account.setAccountTypeMapping(accountTypeMapping);
		//BranchCodeMapping branchCodeMapping = branchCodeRespository.save(account.getBranchCodeMapping());
		//account.setBranchCodeMapping(branchCodeMapping);
		
		logger.info("Inside Account controller Save");
		Customer findCustomer = customerRepository.findOne(customerAccountRel.getCustomerId());
		
		logger.info("findCustomer  :  "+findCustomer.getCustomerId());
		
		Account savedAccount = accountRepository.save(customerAccountRel.getAccount());
		
		logger.info("savedAccount  :  "+savedAccount.getAccountId());
		
		customerAccountRel.setAccount(savedAccount);
		customerAccountRel.setCustomer(findCustomer);
		
		AccountRelationTypeMapping accountRelationTypeMapping = accountRelationTypeMappingRepository.findOne(customerAccountRel.getRelationType());
		
		customerAccountRel.setAccountRelationTypeMapping(accountRelationTypeMapping);
		
		customerAccountRelRepository.save(customerAccountRel);
		
		return "1";
		
	}


	/**
	 * @param customerId
	 */
	public List<Account> getAllAccountsByCusId(Long customerId) {
		List<Account> accounts=new ArrayList<>();
		List <CustomerAccountRel> accountRels = customerAccountRelRepository.findByCustomerId(customerId);
		for(CustomerAccountRel accountRel:accountRels)
		{
			if(!accountRel.getAccount().getAccountStatusMapping().getAccountStatusId().equals(Constants.ACCOUNT_STATUS_CLOSED_ID))
			{
			Account account=accountRel.getAccount();
					accounts.add(account);		
			}
		}
		
		return accounts;	
		
	}


	/**
	 * @param accountNumber
	 * @param accountType
	 * @return
	 */
	public Account getAccountByAccntNoAndType(String accountNumber, Long accountType) {
		Account account=null;
		List<Account> accounts=accountRepository.findByAccountNumberAndAccountTypeAndAccountStatusNot(accountNumber, accountType, Constants.ACCOUNT_STATUS_CLOSED_ID);
		if(!accounts.isEmpty()){
			account=accounts.get(0);
		}
		return account;
	}


	/**
	 * @param accountNumber
	 * @param accountType
	 * @return
	 */
	public List<Account> getAccountBalance(String accountNumber, Long accountType) {
		//Account account=null;
		List<Account> accounts=accountRepository.findByAccountNumberAndAccountTypeAndAccountStatusNot(accountNumber, accountType, Constants.ACCOUNT_STATUS_CLOSED_ID);
		//AccountBalanceResponse accountBalanceResponse=new AccountBalanceResponse();
//		if(!accounts.isEmpty())
//		{
//			account=accounts.get(0);
//			accountBalanceResponse.setAccountBalance(account.getAccountBalance());
//			accountBalanceResponse.setAccountId(account.getAccountId());
//			accountBalanceResponse.setAccountNumber(account.getAccountNumber());
//			accountBalanceResponse.setAccountType(account.getAccountTypeMapping().getType());
//		}
		return accounts;
	}


	/**
	 * @param customerId
	 * @return
	 */
	public List<Account> getAccountsByCusIdAndType(Long customerId,Long accountType) {
		List<Account> accounts=new ArrayList<>();
		logger.info("CustomerId  :  "+customerId +"  accountType  :  "+accountType);
		List <CustomerAccountRel> accountRels = customerAccountRelRepository.findByCustomerId(customerId);
		logger.info("accountRels :  "+accountRels.size());
		
		for(CustomerAccountRel accountRel:accountRels)
		{
			if(accountRel.getAccount().getAccountTypeMapping().getAccountTypeId().equals(accountType)&&!accountRel.getAccount().getAccountStatusMapping().getAccountStatusId().equals(Constants.ACCOUNT_STATUS_CLOSED_ID))
			{
			Account account=accountRel.getAccount();
					accounts.add(account);
					}
		}
		logger.info("accounts ixe  :  "+accounts.size());
		return accounts;	
	}


	/**
	 * @param accountNumber
	 * @param accountType
	 * @return
	 */
//	public int deleteAccount(String accountNumber, Long accountType) {
//		int result=0;
//		Account account=null;
//		try{
//			List<Account> accounts=accountRepository.findByAccountNumberAndAccountType(accountNumber,accountType);
//			if(!accounts.isEmpty()){
//				account=accounts.get(0);
//			CustomerAccountRel customerAccountRel= customerAccountRelRepository.findByAccountId(account.getAccountId());
//			logger.info("customerAccountRel : "+customerAccountRel.getAccount().getAccountId());
//			logger.info("customerAccountRel : "+customerAccountRel.getCustomer().getCustomerId());
//			customerAccountRelRepository.delete(customerAccountRel);
//		    accountRepository.delete(account);
//		    result=1;
//		}}
//		catch(Exception exception)
//		{
//			exception.printStackTrace();
//			result=-1;
//		}
//		return result;
//	}


	/**
	 * @param accountNumber
	 * @param accountType
	 * @return
	 */
	public String update(String accountNumber, Long accountType) {
		String updatedAccountNumber=null;
		Account account=null;
		java.sql.Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());
		try{
			List<Account> accounts=accountRepository.findByAccountNumberAndAccountType(accountNumber,accountType);
			if(!accounts.isEmpty()){
				account=accounts.get(0);
		        account.setTerminationDate(currentTimestamp);
		       AccountStatusMapping accountStatusMapping=accountStatusMappingRepository.findOne(Constants.ACCOUNT_STATUS_CLOSED_ID);
		       account.setAccountStatusMapping(accountStatusMapping);
		       logger.info("accounts ID "+account.getAccountId()+"   Status  : "+accountStatusMapping.getStatus());
		       Account savedAccount = accountRepository.save(account);
		       updatedAccountNumber = savedAccount.getAccountNumber();
	          }
	
            }catch(Exception exception)
		    {
		     exception.printStackTrace();
		     
	         }
		return updatedAccountNumber;
		}


	/**
	 * @param accountNumber
	 * @param accountType
	 * @param accountBalance
	 * @return
	 */
	public String updateBalance(Account account ) {
		String updatedAccountNumber=null;
		try{
			List<Account> accounts=accountRepository.findByAccountNumberAndAccountType(account.getAccountNumber(),account.getAccountType());
			if(!accounts.isEmpty()){
				account=accounts.get(0);
                account.setAccountBalance(account.getAccountBalance());
		       Account savedAccount = accountRepository.save(account);
		       updatedAccountNumber = savedAccount.getAccountNumber();
	          }
	
            }catch(Exception exception)
		    {
		     exception.printStackTrace();
		     
	         }
		return updatedAccountNumber;
	}


	/**
	 * @param customerId
	 * @return
	 */
	public List<Account> getAllClosedAccountsByCusId(Long customerId) {
		List<Account> accounts=new ArrayList<>();
		List <CustomerAccountRel> accountRels = customerAccountRelRepository.findByCustomerId(customerId);
		for(CustomerAccountRel accountRel:accountRels)
		{
			if(accountRel.getAccount().getAccountStatusMapping().getAccountStatusId().equals(Constants.ACCOUNT_STATUS_CLOSED_ID))
			{
			Account account=accountRel.getAccount();
					accounts.add(account);		
			}
		}
		
		return accounts;	
	}
}
	