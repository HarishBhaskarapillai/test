/**
 * 
 * AccountController.java 
 *
 * @author TCS
 * @Version 1.0 - 2016
 * 
 * @Copyright 2016 Tata Consultancy 
 */
package com.tcs.dess.controller;

import java.math.BigDecimal;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.tcs.dess.bean.Account;
import com.tcs.dess.bean.Customer;
import com.tcs.dess.bean.CustomerAccountRel;
import com.tcs.dess.bean.StatusResponse;
import com.tcs.dess.exception.MobilityException;
import com.tcs.dess.service.AccountService;
import com.tcs.dess.utils.Constants;
import com.tcs.dess.utils.ResponseConstructors;

/**
 * Controller to handle batch requests.
 * 
 */
/**
 * This AccountController class holds services for handling Account
 * 
 */
@RestController
@RequestMapping("/accounts")
public class AccountController {

	private static final Logger logger = LoggerFactory
			.getLogger(AccountController.class);

	@Autowired
	AccountService accountService;
	
	@RequestMapping(method = RequestMethod.POST,produces = "application/json")
	public @ResponseBody ResponseEntity<StatusResponse> createAccount(@RequestBody CustomerAccountRel customerAccountRel){
		
		
			try {
				
				accountService.save(customerAccountRel);
			} catch (Exception e) {
				e.printStackTrace();
				logger.error(e.getMessage());
				throw new MobilityException(HttpStatus.INTERNAL_SERVER_ERROR,
						e.getMessage());
			}
			StatusResponse response=new StatusResponse();
			response.setMessage("Success");
			
			return new ResponseEntity<StatusResponse>(response, HttpStatus.OK);
		
	}
	
	
	@RequestMapping(value = "/find", method = RequestMethod.GET,produces = "application/json")
	public @ResponseBody String findAccount(
			@RequestParam(value="account_number", defaultValue = "") String accountNumber,
			@RequestParam(value="account_type", defaultValue = "") Long accountType,
			@RequestParam(value="customer_id", defaultValue = "") Long customerId,
			@RequestParam(value = "view", defaultValue = "all")String view,
			@RequestParam(value = "fields", defaultValue = "all")String fields
			){
		List<Account> accounts = null;
		Account account=new Account();
		logger.info("Account find Invoked");
		logger.info("accountNumber  :  "+accountNumber+"  accountType  :  "+accountType+"  customerId  :  "+customerId);
		try {
			
			if(accountType!=null && customerId==null && !accountNumber.equals("") && !accountType.equals("") && accountNumber != null && accountType!=null){
				logger.info("IF BLK");
				account = accountService.getAccountByAccntNoAndType(accountNumber,accountType);
				fields = Constants.FIND_API_VIEW_ACCOUNT_EXTENDED;
//				AccountDetailsResponse res = new AccountDetailsResponse();
//				res.setAccount(account);
				if(account != null && account.getAccountId()!=null){
					return ResponseConstructors.filterJsonForFieldAndViews(fields,view, account);
				}
				else{
					throw new MobilityException(HttpStatus.NOT_FOUND,
							"Account Not found");
				}
				
			}
			else if(accountType==null && customerId!=null && accountNumber.equals("") &&!customerId.equals("")){
				logger.info("ELSE IF BLK 1");
				accounts = accountService.getAllAccountsByCusId(customerId);
				logger.info("accounts ixe  :  "+accounts.size());
				fields = Constants.FIND_API_VIEW_ACCOUNT_BASIC;
//				AccountResponse res = new AccountResponse();
//				res.setAccounts(accounts);
				if(accounts != null && accounts.size()>0){
					return ResponseConstructors.filterJsonForFieldAndViews(fields,view, accounts);
				}
				else{
					throw new MobilityException(HttpStatus.NOT_FOUND,
							"Account Not found");
				}
				
			}
			else if(accountType!=null && customerId!=null && accountNumber.equals("") && !accountType.equals("") &&!customerId.equals("")){
				logger.info("ELSE IF BLK 2");
				accounts = accountService.getAccountsByCusIdAndType(customerId,accountType);
				logger.info("accounts ixe  :  "+accounts.size());
				fields = Constants.FIND_API_VIEW_ACCOUNT_OTHER;
//				AccountResponse res = new AccountResponse();
//				res.setAccounts(accounts);
				if(accounts != null && accounts.size()>0){
					return ResponseConstructors.filterJsonForFieldAndViews(fields,view, accounts);
				}
				else{
					throw new MobilityException(HttpStatus.NOT_FOUND,
							"Account Not found");
				}
				
			}
			else
			{
				throw new MobilityException(HttpStatus.BAD_REQUEST,
						"Invalid Req Parameters");
			}
			
		} catch (Exception e) {
			//e.printStackTrace();
			logger.error(e.getMessage());
			throw new MobilityException(HttpStatus.INTERNAL_SERVER_ERROR,
					e.getMessage());
		}
		
	}	
	

	@RequestMapping(value="/balance",method = RequestMethod.GET,produces = "application/json")
	public @ResponseBody String  getAccountBalance(
		@RequestParam("account_number") String accountNumber,
		@RequestParam("account_type") Long accountType,
		@RequestParam(value = "view", defaultValue = "all")String view,
		@RequestParam(value = "fields", defaultValue = "all")String fields
			){
		//AccountBalanceResponse accountBalanceResponse=null;
        try{
        	List<Account> accounts = accountService.getAccountBalance(accountNumber,accountType);
        	if(!accounts.isEmpty())
        	{
        		fields = Constants.FIND_API_VIEW_ACCOUNT_BALANCE;
        		return ResponseConstructors.filterJsonForFieldAndViews(fields,view, accounts);
        		//return new ResponseEntity<AccountBalanceResponse>(accountBalanceResponse, HttpStatus.OK);
        	}else
        	{
        		throw new MobilityException(HttpStatus.NO_CONTENT,
    					"No Account Found");
        		//return new ResponseEntity<AccountBalanceResponse>(accountBalanceResponse, HttpStatus.NO_CONTENT);
        	}
        	
        }
        catch (Exception e) {
			e.printStackTrace();
			logger.error(e.getMessage());
			throw new MobilityException(HttpStatus.INTERNAL_SERVER_ERROR,
					e.getMessage());
		}
	}
	@RequestMapping(value="/close",method = RequestMethod.PUT)
	public @ResponseBody ResponseEntity<StatusResponse> closeAccount(
			@RequestParam(value="account_number") String accountNumber,
			@RequestParam(value="account_type") Long accountType
			) {

		String updatedAccountNumber = null;
		logger.info("Account Update invoked for account number : "+accountNumber);
		
		try {
			updatedAccountNumber = accountService.update(accountNumber,accountType);
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new MobilityException(HttpStatus.INTERNAL_SERVER_ERROR,
					e.getMessage());
		}
		
		StatusResponse response=new StatusResponse();
		if(updatedAccountNumber==null)
		{
			response.setMessage("No Account");
			return new ResponseEntity<StatusResponse>(response, HttpStatus.NOT_FOUND);
		}
		response.setMessage("Account Closed : Account Number : "
				+ updatedAccountNumber);
		return new ResponseEntity<StatusResponse>(response, HttpStatus.OK);
	}
	@RequestMapping(value="/balance",method = RequestMethod.PUT)
	public @ResponseBody ResponseEntity<StatusResponse> updateBalance(
			@RequestBody Account account
			) {

		String updatedAccountNumber = null;
		logger.info("Account balance update invoked for account number : "+account.getAccountNumber()+" Balance update value : "+account.getAccountBalance());
		
		try {
			updatedAccountNumber = accountService.updateBalance(account);
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new MobilityException(HttpStatus.INTERNAL_SERVER_ERROR,
					e.getMessage());
		}
		StatusResponse response=new StatusResponse();
		response.setMessage("Account Balance updated : Account Number : "
				+ updatedAccountNumber);

		return new ResponseEntity<StatusResponse>(response, HttpStatus.OK);
	}
	@RequestMapping(value = "/find/close", method = RequestMethod.GET,produces = "application/json")
	public @ResponseBody String findClosedAccount(
			@RequestParam(value="customer_id") Long customerId,
			@RequestParam(value = "view", defaultValue = "all")String view,
			@RequestParam(value = "fields", defaultValue = "all")String fields
			){
		List<Account> accounts = null;
		Account account=new Account();
		logger.info("Closed Account find Invoked");
		logger.info("customerId  :  "+customerId);
		try {
				accounts = accountService.getAllClosedAccountsByCusId(customerId);
				logger.info("accounts ixe  :  "+accounts.size());
				if(!accounts.isEmpty())
				{
				fields = Constants.FIND_API_VIEW_ACCOUNT_EXTENDED;
				return ResponseConstructors.filterJsonForFieldAndViews(fields,view, accounts);
			 }	
			 else
			 {
				throw new MobilityException(HttpStatus.NOT_FOUND,
						"Account Not Found");
			 }
			
		} catch (Exception e) {
			//e.printStackTrace();
			logger.error(e.getMessage());
			throw new MobilityException(HttpStatus.INTERNAL_SERVER_ERROR,
					e.getMessage());
		}
		
	}	
	
}
