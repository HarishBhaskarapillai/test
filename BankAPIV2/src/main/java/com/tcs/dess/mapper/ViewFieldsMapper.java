package com.tcs.dess.mapper;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ViewFieldsMapper {

	private static final Logger logger = LoggerFactory.getLogger(ViewFieldsMapper.class);
	private static final Map<String,String>VIEW_FIELD_MAP;

	static {
		Map<String, String> map = new HashMap<String, String>();
		map.put("user", "userT");
		VIEW_FIELD_MAP = Collections.unmodifiableMap(map);
	}
	
	public static String getFields(String key) {
		String fields = null;
		if (key == null) 
			return null;
		logger.debug("Key: " + key);
		if (VIEW_FIELD_MAP.containsKey(key))
			fields =  VIEW_FIELD_MAP.get(key);
		return fields;
	}
	
}
