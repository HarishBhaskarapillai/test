package com.tcs.dess.bean;

import java.io.Serializable;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonFilter;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import java.sql.Timestamp;
import java.math.BigDecimal;
import java.util.List;


/**
 * The persistent class for the account database table.
 * 
 */
@Entity
@JsonFilter("BANKAPI")
//@JsonIdentityInfo(generator=ObjectIdGenerators.IntSequenceGenerator.class, property="accountId")
@NamedQuery(name="Account.findAll", query="SELECT a FROM Account a")
public class Account implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="account_id")
	private Long accountId;

	@Column(name="account_balance")
	private BigDecimal accountBalance;

	@Column(name="account_number")
	private String accountNumber;

	@Column(name="created_timestamp")
	private Timestamp createdTimestamp;

	@Column(name="created_user")
	private String createdUser;

	@Column(name="last_payment_date")
	private Timestamp lastPaymentDate;

	@Column(name="last_review_date")
	private Timestamp lastReviewDate;

	@Column(name="last_updated_timestamp")
	private Timestamp lastUpdatedTimestamp;

	@Column(name="last_updated_user")
	private String lastUpdatedUser;

	@Column(name="product_code")
	private String productCode;

	@Column(name="termination_date")
	private Timestamp terminationDate;

	//bi-directional many-to-one association to AccountStatusMapping
	@ManyToOne
	@JoinColumn(name="account_status")
	private AccountStatusMapping accountStatusMapping;

	//bi-directional many-to-one association to AccountTypeMapping
	@ManyToOne
	@JoinColumn(name="account_type")
	private AccountTypeMapping accountTypeMapping;

	@Column(name="account_type" , insertable = false, updatable = false)
	private Long accountType;
	
	
	@Column(name="account_status", insertable = false, updatable = false)
	private Long accountStatus;
	
	/**
	 * @return the accountType
	 */
	public Long getAccountType() {
		return accountType;
	}

	/**
	 * @param accountType the accountType to set
	 */
	public void setAccountType(Long accountType) {
		this.accountType = accountType;
	}

	//bi-directional many-to-one association to BranchCodeMapping
	@ManyToOne
	@JoinColumn(name="branch_code")
	private BranchCodeMapping branchCodeMapping;

	//bi-directional many-to-one association to CustomerAccountRel
	@JsonIgnore
	@OneToMany(mappedBy="account", cascade=CascadeType.ALL)
	private List<CustomerAccountRel> customerAccountRels;

	//bi-directional many-to-one association to Transaction
	@JsonIgnore
	@OneToMany(mappedBy="account")
	private List<Transaction> transactions;

	public Account() {
	}

	public Long getAccountId() {
		return this.accountId;
	}

	public void setAccountId(Long accountId) {
		this.accountId = accountId;
	}

	public BigDecimal getAccountBalance() {
		return this.accountBalance;
	}

	public void setAccountBalance(BigDecimal accountBalance) {
		this.accountBalance = accountBalance;
	}

	public String getAccountNumber() {
		return this.accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public Timestamp getCreatedTimestamp() {
		return this.createdTimestamp;
	}

	public void setCreatedTimestamp(Timestamp createdTimestamp) {
		this.createdTimestamp = createdTimestamp;
	}

	public String getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(String createdUser) {
		this.createdUser = createdUser;
	}

	public Timestamp getLastPaymentDate() {
		return this.lastPaymentDate;
	}

	public void setLastPaymentDate(Timestamp lastPaymentDate) {
		this.lastPaymentDate = lastPaymentDate;
	}

	public Timestamp getLastReviewDate() {
		return this.lastReviewDate;
	}

	public void setLastReviewDate(Timestamp lastReviewDate) {
		this.lastReviewDate = lastReviewDate;
	}

	public Timestamp getLastUpdatedTimestamp() {
		return this.lastUpdatedTimestamp;
	}

	public void setLastUpdatedTimestamp(Timestamp lastUpdatedTimestamp) {
		this.lastUpdatedTimestamp = lastUpdatedTimestamp;
	}

	public String getLastUpdatedUser() {
		return this.lastUpdatedUser;
	}

	public void setLastUpdatedUser(String lastUpdatedUser) {
		this.lastUpdatedUser = lastUpdatedUser;
	}

	public String getProductCode() {
		return this.productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public Timestamp getTerminationDate() {
		return this.terminationDate;
	}

	public void setTerminationDate(Timestamp terminationDate) {
		this.terminationDate = terminationDate;
	}

	public AccountStatusMapping getAccountStatusMapping() {
		return this.accountStatusMapping;
	}

	public void setAccountStatusMapping(AccountStatusMapping accountStatusMapping) {
		this.accountStatusMapping = accountStatusMapping;
	}

	public AccountTypeMapping getAccountTypeMapping() {
		return this.accountTypeMapping;
	}

	public void setAccountTypeMapping(AccountTypeMapping accountTypeMapping) {
		this.accountTypeMapping = accountTypeMapping;
	}

	public BranchCodeMapping getBranchCodeMapping() {
		return this.branchCodeMapping;
	}

	public void setBranchCodeMapping(BranchCodeMapping branchCodeMapping) {
		this.branchCodeMapping = branchCodeMapping;
	}

	public List<CustomerAccountRel> getCustomerAccountRels() {
		return this.customerAccountRels;
	}

	public void setCustomerAccountRels(List<CustomerAccountRel> customerAccountRels) {
		for (int i=0; i<customerAccountRels.size(); i++) {
			customerAccountRels.get(i).setAccount(this);
		}
		this.customerAccountRels = customerAccountRels;
	}

	public CustomerAccountRel addCustomerAccountRel(CustomerAccountRel customerAccountRel) {
		getCustomerAccountRels().add(customerAccountRel);
		customerAccountRel.setAccount(this);

		return customerAccountRel;
	}

	public CustomerAccountRel removeCustomerAccountRel(CustomerAccountRel customerAccountRel) {
		getCustomerAccountRels().remove(customerAccountRel);
		customerAccountRel.setAccount(null);

		return customerAccountRel;
	}

	public List<Transaction> getTransactions() {
		return this.transactions;
	}

	public void setTransactions(List<Transaction> transactions) {
		this.transactions = transactions;
	}

	public Transaction addTransaction(Transaction transaction) {
		getTransactions().add(transaction);
		transaction.setAccount(this);

		return transaction;
	}

	public Transaction removeTransaction(Transaction transaction) {
		getTransactions().remove(transaction);
		transaction.setAccount(null);

		return transaction;
	}

	/**
	 * @return the accountStatusMappingId
	 */
	public Long getAccountStatus() {
		return accountStatus;
	}

	/**
	 * @param accountStatusMappingId the accountStatusMappingId to set
	 */
	public void setAccountStatus(Long accountStatus) {
		this.accountStatus = accountStatus;
	}

	

}