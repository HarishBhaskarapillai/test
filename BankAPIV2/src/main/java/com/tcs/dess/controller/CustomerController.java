/**
 * 
 * CustomerController.java 
 *
 * @author TCS
 * @Version 1.0 - 2016
 * 
 * @Copyright 2016 Tata Consultancy 
 */
package com.tcs.dess.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.PathParam;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.tcs.dess.bean.Customer;
import com.tcs.dess.bean.CustomerResponse;
import com.tcs.dess.bean.RequestVO;
import com.tcs.dess.bean.StatusResponse;
import com.tcs.dess.bean.User;
import com.tcs.dess.bean.UserT;
import com.tcs.dess.enums.Status;
import com.tcs.dess.exception.MobilityException;
import com.tcs.dess.service.CustomerService;
import com.tcs.dess.service.UserService;
import com.tcs.dess.utils.Constants;
import com.tcs.dess.utils.ResponseConstructors;
import com.tcs.dess.utils.SessionUtil;
import com.wordnik.swagger.annotations.Api;


/**
 * This CustomerController class holds services for handling Customer
 * 
 */
@RestController
@RequestMapping("/customers")
public class CustomerController {

	private static final Logger logger = LoggerFactory
			.getLogger(CustomerController.class);

	@Autowired
	UserService userService;
	
	
	@Autowired
	CustomerService customerService;

	@RequestMapping(method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<StatusResponse> createCustomer(
			@RequestBody Customer customer,HttpServletRequest httpServletRequest) {
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		String userName=null;
		if (principal instanceof UserDetails) {
			  userName = ((UserDetails)principal).getUsername();
			} else {
			  userName = principal.toString();
			}
		
		String response = null;
		logger.info("Create customer Invoked");

		try {
			User user = userService.findByUserName(userName);
			if(!user.getRole().equalsIgnoreCase("ADMIN"))
			{
				throw new MobilityException(HttpStatus.UNAUTHORIZED,
						"NOT AUTHORIZED");
			}
			response = customerService.save(customer);
			
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new MobilityException(HttpStatus.INTERNAL_SERVER_ERROR,
					e.getMessage());
		}
		StatusResponse statusResponse=new StatusResponse();
		statusResponse.setMessage("Customer Created : Customer Id : "
				+ response);
		return new ResponseEntity<StatusResponse>(statusResponse, HttpStatus.OK);
	}

	
	@RequestMapping(method = RequestMethod.PUT)
	public @ResponseBody ResponseEntity<StatusResponse> updateCustomer(
			@RequestBody Customer customer) {
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		String userName=null;
		if (principal instanceof UserDetails) {
			  userName = ((UserDetails)principal).getUsername();
			} else {
			  userName = principal.toString();
			}
		String response = null;
		logger.info("customer Update invoked for customerId : "+customer.getCustomerId());
		
		try {
			User user = userService.findByUserName(userName);
			if(!user.getRole().equalsIgnoreCase("ADMIN"))
			{
				throw new MobilityException(HttpStatus.UNAUTHORIZED,
						"NOT AUTHORIZED");
			}
			response = customerService.update(customer);
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new MobilityException(HttpStatus.INTERNAL_SERVER_ERROR,
					e.getMessage());
		}
		StatusResponse statusResponse=new StatusResponse();
		statusResponse.setMessage("Customer Updated : Customer Id : "
				+ customer.getCustomerId());

		return new ResponseEntity<StatusResponse>(statusResponse, HttpStatus.OK);
	}

	@RequestMapping(value = "/find", method = RequestMethod.GET,produces = "application/json")
	public @ResponseBody String findCustomer(
			@RequestParam(value="firstName", defaultValue = "") String firstName,
			@RequestParam(value="lastName", defaultValue = "") String lastName,
			@RequestParam(value="identificationType", defaultValue = "") Long identificationType,
			@RequestParam(value="identificationValue", defaultValue = "") String identificationValue,
			@RequestParam(value="contactType", defaultValue = "") Long contactType,
			@RequestParam(value="contactValue", defaultValue = "") String contactValue,
			@RequestParam(value = "fields", defaultValue = "all") String fields,
			@RequestParam(value = "view", defaultValue = "all") String view) {
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		String userName=null;
		if (principal instanceof UserDetails) {
			  userName = ((UserDetails)principal).getUsername();
			} else {
			  userName = principal.toString();
			}
		List<Customer> customers = null;
		
		logger.info("customer find Invoked");
		
		try {
			User user = userService.findByUserName(userName);
			if(!user.getRole().equalsIgnoreCase("ADMIN"))
			{
				throw new MobilityException(HttpStatus.UNAUTHORIZED,
						"NOT AUTHORIZED");
			}
			logger.info("FirstName : "+firstName+"  LastName  :  "+lastName+" identificationType :  "+identificationType
					+"  identificationValue : "+identificationValue+"  contactType : "+contactType+"   contactValue : "+contactValue);
			
			if(!firstName.equals("") && !lastName.equals("") && firstName != null && lastName!=null){
				logger.info("FN and LN");
				customers = customerService.findByFirstNameAndLastName(firstName,lastName);
			}
			else if(identificationValue != null && identificationType != null && !identificationType.equals("") &&  !identificationValue.equals("") ){
				logger.info("Identification");
				customers = customerService.findByIdentification(identificationType,identificationValue);
			}
			else if(contactValue != null && contactType != null && !contactType.equals("") && !contactValue.equals("")){
				customers = customerService.findByContact(contactType,contactValue);
			}
			
			
			CustomerResponse res = new CustomerResponse();
			res.setCustomers(customers);
			
			
			if(fields.equals("all") && !firstName.equals("")){
				fields = Constants.FIND_API_VIEW_GENERIC;
			}
			else if(fields.equals("all") && !identificationValue.equals("")){
				fields = Constants.FIND_API_VIEW_GENERIC+","+Constants.FIND_API_VIEW_IDENTIFICATION;
			}
			else if(fields.equals("all") && !contactValue.equals("")){
				fields = Constants.FIND_API_VIEW_GENERIC+","+Constants.FIND_API_VIEW_CONTACT;
			}
			
			logger.info("List of fields for filtering : "+fields);
			
			return ResponseConstructors.filterJsonForFieldAndViews(fields,view, res);
			
		} catch (Exception e) {
			//e.printStackTrace();
			logger.error(e.getMessage());
			throw new MobilityException(HttpStatus.INTERNAL_SERVER_ERROR,
					e.getMessage());
		}
		
	}

	@RequestMapping(value = "/{customerId}", method = RequestMethod.GET,produces = "application/json")
	public @ResponseBody String getCustomerProfile(
			@PathVariable("customerId") String customerId,
			@RequestParam(value = "fields", defaultValue = "all") String fields,
			@RequestParam(value = "view", defaultValue = "all") String view) {
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		String userName=null;
		if (principal instanceof UserDetails) {
			  userName = ((UserDetails)principal).getUsername();
			} 
		logger.info(userName);
		try {
			User user = userService.findByUserName(userName);
			if(!user.getRole().equalsIgnoreCase("ADMIN"))
			{
				throw new MobilityException(HttpStatus.UNAUTHORIZED,
						"NOT AUTHORIZED");
			}
			logger.info("GetCustomerProfile Invoked for customer Id  :  "+customerId);
			Customer res = customerService.getCustomerProfile(new Long(
					customerId));
			return ResponseConstructors.filterJsonForFieldAndViews(fields,view, res);

		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new MobilityException(HttpStatus.INTERNAL_SERVER_ERROR,
					e.getMessage());
		}

	}

}
