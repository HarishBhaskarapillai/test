/**
 * 
 * LoginHistoryRepository.java 
 *
 * @author TCS
 * @Version 1.0 - 2016
 * 
 * @Copyright 2016 Tata Consultancy 
 */
package com.tcs.dess.data.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.tcs.dess.bean.Account;


/**
 * This UserRepository class holds the CRUD services of UserT bean
 * 
 */
@Repository
public interface AccountRepository extends CrudRepository<Account, Long> {

	/**
	 * @param accountNumber
	 * @param accountType
	 * @return
	 */
	List<Account> findByAccountNumberAndAccountTypeAndAccountStatusNot(String accountNumber, Long accountType,Long accountStatus);

	/**
	 * @param accountNumber
	 * @param accountType
	 * @return
	 */
	List<Account> findByAccountNumberAndAccountType(String accountNumber, Long accountType);
	
	
	
}
