/**
 * 
 * LoginHistoryRepository.java 
 *
 * @author TCS
 * @Version 1.0 - 2016
 * 
 * @Copyright 2016 Tata Consultancy 
 */
package com.tcs.dess.data.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.tcs.dess.bean.User;

/**
 * This UserRepository class holds the CRUD services of UserT bean
 * 
 */
@Repository
public interface UserRepository extends CrudRepository<User, Long> {
	
	/**
	 * Method to get the user details for the userName
	 * 
	 * @param userName
	 * @return User
	 */
	User findByUserName(String userName);

	/**
	 * Method to get the user details for the userId
	 * 
	 * @param userId
	 * @return User
	 */
	User findByUserId(Long userId);

	/**
	 * @param userName
	 * @param password
	 * @return
	 */
	User findByUserNameAndPassword(String userName, String password);
	
}
