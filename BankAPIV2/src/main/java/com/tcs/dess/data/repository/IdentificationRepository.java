/**
 * 
 * LoginHistoryRepository.java 
 *
 * @author TCS
 * @Version 1.0 - 2016
 * 
 * @Copyright 2016 Tata Consultancy 
 */
package com.tcs.dess.data.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.tcs.dess.bean.Contact;
import com.tcs.dess.bean.Customer;
import com.tcs.dess.bean.Identification;
import com.tcs.dess.bean.UserT;

/**
 * This UserRepository class holds the CRUD services of UserT bean
 * 
 */
@Repository
public interface IdentificationRepository extends CrudRepository<Identification, Long> {
	
	List<Identification> findByIdentificationTypeAndIdentificationValue(Long identificationType, String identificationValue);

	/**
	 * @param identificationValue
	 * @param identificationType
	 * @return
	 */
	List<Identification> findByIdentificationValueAndIdentificationType(
			String identificationValue, Long identificationType);
	
	
}
