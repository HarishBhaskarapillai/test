/**
 * 
 * Switch.java 
 *
 * @author TCS
 * @Version 1.0 - 2016
 * 
 * @Copyright 2016 Tata Consultancy 
 */
package com.tcs.dess.enums;

/**
 * This Switch enum holds the constants for switch
 * 
 */
public enum Switch {
	
	ON, OFF;
	
	public boolean checkSwitch(String value) {
		return this.name().equals(value);
	}
	
}
