///**
// * 
// * UserDetailsController.java 
// *
// * @author TCS
// * @Version 1.0 - 2016
// * 
// * @Copyright 2016 Tata Consultancy 
// */
//package com.tcs.dess.controller;
//
//import java.sql.Timestamp;
//import java.util.ArrayList;
//import java.util.List;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpSession;
//
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.http.HttpHeaders;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.ResponseEntity;
//import org.springframework.security.core.session.SessionInformation;
//import org.springframework.security.core.session.SessionRegistry;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestMethod;
//import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.bind.annotation.ResponseBody;
//import org.springframework.web.bind.annotation.RestController;
//
//import com.tcs.dess.bean.LoginHistoryT;
//import com.tcs.dess.bean.User;
//import com.tcs.dess.bean.UserT;
//import com.tcs.dess.enums.Status;
//import com.tcs.dess.exception.MobilityException;
//import com.tcs.dess.service.UserService;
//import com.tcs.dess.utils.SessionUtil;
//import com.tcs.dess.utils.ResponseConstructors;
//
//import eu.bitwalker.useragentutils.Browser;
//import eu.bitwalker.useragentutils.OperatingSystem;
//import eu.bitwalker.useragentutils.UserAgent;
//
///**
// * This class deals with user details 
// * 
// * @author tcs
// *
// */
//@RestController
//@RequestMapping("/user")
//public class UserDetailsController {
//
//	private static final Logger logger = LoggerFactory
//			.getLogger(UserDetailsController.class);
//
//	@Autowired
//	UserService userService;
//
//	@Autowired
//	private SessionRegistry sessionRegistry;
//	
//	@Value("${maximum.concurrent.user}")
//	private int maxActiveSession;
//
//
//	/**
//	 * This method is used to validate User Login Also saves Login SessionId,
//	 * Date Time, Device, Browser details
//	 * 
//	 * @param userName
//	 *            is the login user name.
//	 * @return Login response.
//	 */
//	@RequestMapping(value = "/login", method = RequestMethod.POST)
//	public @ResponseBody ResponseEntity<String> userLogin(
//			HttpServletRequest httpServletRequest,
//			@RequestParam(value = "fields", defaultValue = "all") String fields,
//			@RequestParam(value = "view", defaultValue = "") String view,
//			@RequestParam(value = "appVersion", defaultValue = "") String appVersion)
//			throws MobilityException {
//
//		logger.info("starting UserDetailsController /user/login POST");
//		try {
//			List<SessionInformation> activeSessions = null;
//			logger.info("httpServletRequest sessionId:"
//					+ httpServletRequest.getSession(false).getId());
//			activeSessions = new ArrayList<>();
//			
//			for (Object principal : sessionRegistry.getAllPrincipals()) {
//				activeSessions.addAll(sessionRegistry.getAllSessions(principal,
//						false));
//			}
//			logger.info("active session size:" + activeSessions.size());
//			 /*
//			 * Max active session is commented as the sessions are not expired when timeout occurs
//			 * i.e when the user leaves the application without logout.
//			 */
//			if (maxActiveSession >= activeSessions.size()) {
//				User user = userService.findByUserId(SessionUtil
//						.getCurrentUserDetails().getUserId());
//				if (user != null) {
//					// Log Username for debugging
//					HttpSession session = httpServletRequest.getSession(false);
//					if (session == null) {
//						session = httpServletRequest.getSession();
//					}
//
//					// Check if the User has already logged in with the same
//					// session
////					if (userService.findByUserIdAndSessionId(user.getUserId(),
////							session.getId()) != null) {
////						throw new MobilityException(HttpStatus.FORBIDDEN,
////								"User has already logged in with the same session");
////					}
//
//					logger.info("Session Timeout: {}",
//							session.getMaxInactiveInterval());
////
////					// Get Last Login Time
////					Timestamp lastLogin = userService.getUserLastLogin(user
////							.getUserId());
////					if (lastLogin != null)
////						user.setLastLogin(lastLogin);
//
//					// Get Browser, Device details from request header
////					logger.info("UserAgent : "
////							+ httpServletRequest.getHeader("User-Agent"));
////					UserAgent userAgent = UserAgent
////							.parseUserAgentString(httpServletRequest
////									.getHeader("User-Agent"));
////					Browser browser = null;
////					String browserName = null;
////					String browserVersion = null;
////					if (userAgent.getBrowser() != null) {
////						browser = userAgent.getBrowser();
////						if (browser != null && browser.getName() != null)
////							browserName = browser.getName();
////					}
////					if (userAgent.getBrowserVersion() != null)
////						browserVersion = userAgent.getBrowserVersion()
////								.getVersion();
////					logger.info("Browser: {}, Version: {}", browserName,
////							browserVersion);
////
////					// Get OS details
////					OperatingSystem os = null;
////					String osName = null;
////					short osVersion = 0;
////					if (userAgent.getOperatingSystem() != null) {
////						os = userAgent.getOperatingSystem();
////						if (os != null) {
////							osVersion = os.getId();
////							if (os.getName() != null)
////								osName = os.getName();
////						}
////					}
////
////					logger.info("OS: {}, Version: {}", os, (byte) osVersion);
////
////					// Get Device details
////					String device = null;
////					if (os.getDeviceType() != null) {
////						if (os.getDeviceType().getName() != null)
////							device = os.getDeviceType().getName();
////					}
////					logger.info("Device: {}", device);
//
////					// Save current login session
////					LoginHistoryT loginHistory = new LoginHistoryT();
////					loginHistory.setUserId(user.getUserId());
////					loginHistory.setSessionId(session.getId());
////					loginHistory.setBrowser(browserName);
////					loginHistory.setBrowserVersion(browserVersion);
////					loginHistory.setOs(osName);
////					loginHistory.setOsVersion(Integer
////							.toString((byte) osVersion));
////					loginHistory.setDevice(device);
////					if (appVersion != null && !appVersion.isEmpty()) {
////						logger.info("App Version: {}", appVersion);
////						loginHistory.setAppVersion(appVersion);
////					}
////
////					if (!userService.addLoginHistory(loginHistory)) {
////						throw new MobilityException(
////								HttpStatus.INTERNAL_SERVER_ERROR,
////								"Could not save Login History");
////					}
//				}
//
//
//				logger.info("Ending UserDetailsController /user/login POST");
//				return new ResponseEntity<String>(
//						ResponseConstructors.filterJsonForFieldAndViews(fields,
//								view, user), new HttpHeaders(), HttpStatus.OK);
//			} else {
//				HttpSession maxreq_session = httpServletRequest
//						.getSession(false);
//				if (maxreq_session != null) {
//
//					sessionRegistry.removeSessionInformation(httpServletRequest
//							.getSession(false).getId());
//					maxreq_session.invalidate();
//				}
//				logger.info("Maximum number of users reached.Please try after sometime");
//				throw new MobilityException(
//						HttpStatus.SERVICE_UNAVAILABLE,
//						"Maximum number of users reached.Please try after sometime");
//			}
//		} catch (MobilityException e) {
//			throw e;
//		} catch (Exception e) {
//			logger.error("Backend Error while login process");
//			e.printStackTrace();
//			throw new MobilityException(HttpStatus.INTERNAL_SERVER_ERROR,
//					"Backend Error while login process");
//		}
//	}
//
//	/**
//	 * @param httpServletRequest
//	 * @return
//	 * @throws MobilityException
//	 */
//	@RequestMapping(value = "/logout", method = RequestMethod.GET)
//	public @ResponseBody ResponseEntity<String> userLogout(
//			HttpServletRequest httpServletRequest) throws MobilityException {
//		try {
//			logger.info("Starting UserDetailsController /user/logout GET");
//			Status status = new Status();
//			HttpSession session = httpServletRequest.getSession(false);
//			if (session != null) {
//				logger.info("Logging out User Session: {}", session.getId());
//				session.invalidate();
//				sessionRegistry.removeSessionInformation(session.getId());
//				status.setStatus(Status.SUCCESS, "Session logged out");
//			} else {
//				throw new MobilityException(HttpStatus.NOT_FOUND,
//						"No valid session to log out");
//			}
//			logger.info("Ending UserDetailsController /user/logout GET");
//			return new ResponseEntity<String>(
//					ResponseConstructors.filterJsonForFieldAndViews("all", "",
//							status), HttpStatus.OK);
//		} catch (MobilityException e) {
//			throw e;
//		} catch (IllegalStateException e) {
//			logger.error("Invalid session.");
//			throw new MobilityException(HttpStatus.UNAUTHORIZED,
//					"Invalid session");
//		} catch (Exception e) {
//			logger.error("Backend Error while logout process");
//			throw new MobilityException(HttpStatus.INTERNAL_SERVER_ERROR,
//					"Backend Error while logout process");
//		}
//	}
//
//}
