/**
 * 
 * JobName.java 
 *
 * @author TCS
 * @Version 1.0 - 2016
 * 
 * @Copyright 2016 Tata Consultancy 
 */
package com.tcs.dess.enums;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.tcs.dess.mapper.JobNameDeserializer;

/**
 * This JobName enum contains the jobs names in the application
 * 
 */
@JsonFormat(shape=JsonFormat.Shape.OBJECT)
@JsonDeserialize(using=JobNameDeserializer.class)
public enum JobName {
	
        userUpload("userUpload");
		
		private String job;
		
		@JsonProperty("job")
		public String getJob() {
			return job;
		}
		
		private JobName(String job) {
			this.job = job;
		}

}
