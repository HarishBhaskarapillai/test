/**
 * 
 * AccountService.java 
 *
 * @author TCS
 * @Version 1.0 - 2016
 * 
 * @Copyright 2016 Tata Consultancy 
 */
package com.tcs.dess.service;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tcs.dess.bean.Account;
import com.tcs.dess.bean.AccountRelationTypeMapping;
import com.tcs.dess.bean.AccountStatusMapping;
import com.tcs.dess.bean.Address;
import com.tcs.dess.bean.Customer;
import com.tcs.dess.bean.CustomerAccountRel;
import com.tcs.dess.bean.CustomerAddressRel;
import com.tcs.dess.bean.Payee;
import com.tcs.dess.bean.Transaction;
import com.tcs.dess.bean.Transfer;
import com.tcs.dess.data.repository.AccountRelationTypeMappingRepository;
import com.tcs.dess.data.repository.AccountRepository;
import com.tcs.dess.data.repository.AccountStatusMappingRepository;
import com.tcs.dess.data.repository.CustomerAccountRelRepository;
import com.tcs.dess.data.repository.CustomerRepository;
import com.tcs.dess.data.repository.PayeeRepository;
import com.tcs.dess.data.repository.TransactionRepository;
import com.tcs.dess.data.repository.TransferRepository;
import com.tcs.dess.utils.Constants;

/**
 * 
 * This service handles functionalities related to Account
 *
 */
@Service
@Transactional
public class TransferService {

	@SuppressWarnings("unused")
	private static final Logger logger = LoggerFactory
			.getLogger(TransferService.class);
	
		
	
	@Autowired
	private AccountRepository accountRepository;
	
	@Autowired
	private CustomerRepository customerRepository;
	
	@Autowired
	private PayeeRepository payeeRepository;


	@Autowired
	private TransferRepository transferRepository;
	
	@Autowired
	private TransactionService transactionService;

	
	/**
	 * @param payee
	 */
	public String addPayee(Payee payee) {
		
		Long customerId = payee.getCustomer().getCustomerId();
		String toAccountNumber = payee.getAccount().getAccountNumber();
		Long toAccountType = payee.getAccount().getAccountTypeMapping().getAccountTypeId();
	
		logger.info("To Acc No  :  "+toAccountNumber+"  To Ac Type  : "+toAccountType);
		
		Customer customer = customerRepository.findOne(customerId);
		List<Account> toAccounts = accountRepository.findByAccountNumberAndAccountType(toAccountNumber, toAccountType);
		
		if(customer!=null && !toAccounts.isEmpty()){	
			payee.setCustomer(customer);
			payee.setAccount(toAccounts.get(0));
			payee.setPayeeNickname(payee.getPayeeNickname());
			payee.setAddedDate(new Timestamp(new Date().getTime()));
			Payee addedPayee = payeeRepository.save(payee);
			return ""+addedPayee.getPayeeId();
		}
		else
		{
			return "Customer / Account Not found";
		}
		
	}

	/**
	 * @param accountNumber
	 * @param accountType
	 */
	public List<Payee> getPayee(Long customerId) {
		logger.info("Customer Id  :  "+customerId);
		
		List<Payee> listOfPayee = new ArrayList<Payee>();
		
		Customer customer = customerRepository.findOne(customerId);
		
		if(customer != null){			
			listOfPayee = payeeRepository.findByCustomerId(customerId);
			logger.info("listOfPayee  : "+listOfPayee.size());
			return listOfPayee;
		}
		else{
			logger.info("Customer Not Found");
			return listOfPayee;
		}
		
	}

	/**
	 * @param transfer
	 */
	public String addTransfer(Transfer transfer) {
		transfer.setTransferDate(new Timestamp(new Date().getTime()));
		
		logger.info("transfer From Acc:  "+transfer.getAccount().getAccountId());
		logger.info("Transfer To acc   :  "+transfer.getPayee().getAccount().getAccountId());
		
		Account fromAccount = accountRepository.findOne(transfer.getAccount().getAccountId());
		List<Payee> payee = payeeRepository.findByAccountId(transfer.getPayee().getAccount().getAccountId());
		
		
		if(fromAccount != null && !payee.isEmpty()){
			transfer.setPayee(payee.get(0));
			transfer.setTransferDate(new Timestamp(new Date().getTime()));
			transfer.setTransferType("INTERNAL");
			transfer.setAccount(fromAccount);
			
			//Debit from Soruce Account
			Transaction debitTransaction = new Transaction();
			debitTransaction.setTransactionType(Constants.TRANSACTION_TYPE_DEBIT);
			debitTransaction.setAmount(transfer.getTransferAmount());
			debitTransaction.setAccount(fromAccount);
			debitTransaction.setDescription("INTRA BANK TRANSFER SENT");
			debitTransaction.setTransactionDate(new Timestamp(new Date().getTime()));
			String debitTxnResult = transactionService.postTransaction(debitTransaction);
			
			if(!debitTxnResult.equals(Constants.TRANSACTION_NOT_ENOUGH_BALANCE)){
			
			Transaction creditTransaction = new Transaction();
			creditTransaction.setTransactionType(Constants.TRANSACTION_TYPE_CREDIT);
			creditTransaction.setAmount(transfer.getTransferAmount());
			creditTransaction.setAccount(payee.get(0).getAccount());
			creditTransaction.setDescription("INTRA BANK TRANSFER RECEIVED");
			creditTransaction.setTransactionDate(new Timestamp(new Date().getTime()));
			transactionService.postTransaction(creditTransaction);
			
			
			
			Transfer savedTransfer = transferRepository.save(transfer);
			return ""+savedTransfer.getTransferId();
			}
			else{
				return Constants.TRANSACTION_NOT_ENOUGH_BALANCE;
			}
		}
		
		return "No Accounts Found";
	}

	/**
	 * @param accountNumber
	 * @param accountType
	 */
	public List<Transfer> getTransfers(String accountNumber, Long accountType) {
				
		List<Transfer> transfers = new ArrayList<Transfer>();
				
		List<Account> accounts = accountRepository.findByAccountNumberAndAccountType(accountNumber, accountType);
		
			
		if(!accounts.isEmpty()){
			transfers = transferRepository.findByAccountIdOrderByTransferDateDesc(accounts.get(0).getAccountId());
		}
		else{
			return transfers;
		}
		logger.info("Transfers size  : "+transfers.size());
		return transfers;		
	}
	
	

	
}
	