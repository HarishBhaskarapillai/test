package com.tcs.dess.bean;

import java.util.List;

public class MappingTable
{
	 private String name;
	private List<CodeValue> codeValue;

   

    public List<CodeValue> getCodeValue ()
    {
        return codeValue;
    }

    public void setCodeValue (List<CodeValue> codeValue)
    {
        this.codeValue = codeValue;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

   
}
			