/**
 * 
 * ProductController.java 
 *
 * @author TCS
 * @Version 1.0 - 2016
 * 
 * @Copyright 2016 Tata Consultancy 
 */
package com.tcs.dess.controller;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.PathParam;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.tcs.dess.bean.Customer;
import com.tcs.dess.bean.RequestVO;
import com.tcs.dess.bean.UserT;
import com.tcs.dess.enums.Status;
import com.tcs.dess.exception.MobilityException;
import com.tcs.dess.service.AccountService;
import com.tcs.dess.service.CustomerService;
import com.tcs.dess.service.ProductService;
import com.tcs.dess.utils.ResponseConstructors;
import com.tcs.dess.utils.SessionUtil;

/**
 * Controller to handle batch requests.
 * 
 */
/**
 * This ProductController class holds services for handling Products
 * 
 */
@RestController
@RequestMapping("/products")
public class ProductController {

	private static final Logger logger = LoggerFactory
			.getLogger(ProductController.class);

	@Autowired
	ProductService productService;
	
	@RequestMapping(value="{account_type}",method = RequestMethod.GET)
	public String retrieveProductListBasedOnType(
			@PathParam("account_type") String accountType
			){
		
		
		return "";
	}
	
	@RequestMapping(value="{product_code}",method = RequestMethod.GET)
	public String retrieveProductDetails(
			@PathParam("product_code") String productCode
			){
		
		
		return "";
	}
	
}
