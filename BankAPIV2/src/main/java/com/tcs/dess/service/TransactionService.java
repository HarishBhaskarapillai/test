/**
 * 
 * TransactionService.java 
 *
 * @author TCS
 * @Version 1.0 - 2016
 * 
 * @Copyright 2016 Tata Consultancy 
 */
package com.tcs.dess.service;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tcs.dess.bean.Account;
import com.tcs.dess.bean.Customer;
import com.tcs.dess.bean.LoginHistoryT;
import com.tcs.dess.bean.Transaction;
import com.tcs.dess.bean.UserT;
import com.tcs.dess.data.repository.AccountRepository;
import com.tcs.dess.data.repository.CustomerRepository;
import com.tcs.dess.data.repository.LoginHistoryRepository;
import com.tcs.dess.data.repository.TransactionRepository;
import com.tcs.dess.data.repository.UserRepository;
import com.tcs.dess.utils.Constants;

/**
 * 
 * This service handles functionalities related to Transaction
 *
 */
@Service
public class TransactionService {

	private static final Logger logger = LoggerFactory
			.getLogger(TransactionService.class);
	
		
	@Autowired
	private TransactionRepository transactionRepository;
	
	@Autowired
	private AccountRepository accountRepository;


	/**
	 * @param transaction
	 * @return
	 */
	public String postTransaction(Transaction transaction) {
		logger.info(""+transaction.getAccount().getAccountId());
		logger.info(""+transaction.getTransactionType());
		logger.info(""+transaction.getAmount());
		
		
		if(transaction.getTransactionType().equalsIgnoreCase(Constants.TRANSACTION_TYPE_CREDIT))
		{
			Account account = accountRepository.findOne(transaction.getAccount().getAccountId());
			logger.info("Transaction Found Acc : "+account.getAccountId());			
			BigDecimal balance = account.getAccountBalance();
			logger.info("Balance Before update : "+ balance);
			if(balance != null){
				balance = balance.add(transaction.getAmount());
			}
			else{
				balance = transaction.getAmount();
			}
			logger.info("Balance After update : "+ balance);
			account.setAccountBalance(balance);
			accountRepository.save(account);
		}
		else if(transaction.getTransactionType().equalsIgnoreCase(Constants.TRANSACTION_TYPE_DEBIT))
		{
			Account account = accountRepository.findOne(transaction.getAccount().getAccountId());
			logger.info("Transaction Found Acc : "+account.getAccountId());		
			BigDecimal balance = account.getAccountBalance();
			logger.info("Balance Before update : "+ balance);
			
			if(balance != null && ( balance.compareTo(transaction.getAmount()) )>=0)
			{
				balance = balance.subtract(transaction.getAmount());
				logger.info("Balance After update : "+ balance);
			}
			else{
				return Constants.TRANSACTION_NOT_ENOUGH_BALANCE;
			}
			account.setAccountBalance(balance);
			accountRepository.save(account);
			
		}
		Transaction savedTransaction = transactionRepository.save(transaction);
		
		return savedTransaction.getTransactionId().toString();
	}


	/**
	 * @param accountNumber
	 * @param accountType
	 * @return
	 */
	public List<Transaction> getMiniStatement(String accountNumber, Long accountType) {
		
		logger.info("TransactionService getMiniStatement Invoked....");
		List<Account> accounts = accountRepository.findByAccountNumberAndAccountType(accountNumber, accountType);
		 
		List<Transaction> response = new ArrayList<Transaction>();
		
		if(accounts.size()>0){
			response = transactionRepository.findByAccountIdOrderByTransactionDateDesc(accounts.get(0).getAccountId()); 			
		}
		
		 logger.info("Response Size  : "+response.size());
		 
		return response;
	}


	/**
	 * @param accountNumber
	 * @param accountType
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public List<Transaction> getDetailedStatement(String accountNumber, Long accountType,
			Date startDate, Date endDate) {
		
		 List<Account> accounts = accountRepository.findByAccountNumberAndAccountType(accountNumber,accountType);
		 
		 List<Transaction> response = new ArrayList<Transaction>();
		 
		 if(accounts.size()>0){
			 Timestamp fromDateTs = new Timestamp(startDate.getTime());
			 Timestamp toDateTs = new Timestamp(endDate.getTime()
			 + Constants.ONE_DAY_IN_MILLIS - 1);
			
			 logger.info("fromDateTs : "+fromDateTs+"  toDateTs :  "+toDateTs);
			 
			 response = transactionRepository.findByTransactionDateBetweenAndAccountIdOrderByTransactionDateDesc(fromDateTs,toDateTs,accounts.get(0).getAccountId());
		 }
		 
		 logger.info("Response Size  : "+response.size());
		 
		return response;
	}
	
	/**
	 * This method is used to retrieve user details based on userId
	 * @param userId
	 * @return
	 * @throws Exception
	 */
	
}