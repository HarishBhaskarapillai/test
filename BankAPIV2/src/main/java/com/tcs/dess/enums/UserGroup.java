/**
 * 
 * UserGroup.java 
 *
 * @author TCS
 * @Version 1.0 - 2016
 * 
 * @Copyright 2016 Tata Consultancy 
 */
package com.tcs.dess.enums;

/**
 * This UserGroup class holding different groups supported
 * 
 */
public enum UserGroup {
	
	Admin("admin"), User("user");
	
	private String groupName;
	
	private UserGroup(String groupName) {
		this.groupName = groupName;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroup(String groupName) {
		this.groupName = groupName;
	}
	
}
