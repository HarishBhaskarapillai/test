package com.tcs.dess.bean;

import java.io.Serializable;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonFilter;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the account_relation_type_mapping database table.
 * 
 */
@Entity
@JsonFilter("BANKAPI")
@JsonIdentityInfo(generator=ObjectIdGenerators.IntSequenceGenerator.class, property="accountRelationTypeId")
@Table(name="account_relation_type_mapping")
@NamedQuery(name="AccountRelationTypeMapping.findAll", query="SELECT a FROM AccountRelationTypeMapping a")
public class AccountRelationTypeMapping implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="account_relation_type_id")
	private Long accountRelationTypeId;

	private String status;
	
	private String description;
	
	@Column(name="created_timestamp")
	private Timestamp createdTimestamp;

	@Column(name="created_user")
	private String createdUser;



	@Column(name="last_updated_timestamp")
	private Timestamp lastUpdatedTimestamp;

	@Column(name="last_updated_user")
	private String lastUpdatedUser;

	

	//bi-directional many-to-one association to CustomerAccountRel
	@JsonIgnore
	@OneToMany(mappedBy="accountRelationTypeMapping",fetch=FetchType.LAZY)
	private List<CustomerAccountRel> customerAccountRels;

	public AccountRelationTypeMapping() {
	}

	public Long getAccountRelationTypeId() {
		return this.accountRelationTypeId;
	}

	public void setAccountRelationTypeId(Long accountRelationTypeId) {
		this.accountRelationTypeId = accountRelationTypeId;
	}

	public Timestamp getCreatedTimestamp() {
		return this.createdTimestamp;
	}

	public void setCreatedTimestamp(Timestamp createdTimestamp) {
		this.createdTimestamp = createdTimestamp;
	}

	public String getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(String createdUser) {
		this.createdUser = createdUser;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Timestamp getLastUpdatedTimestamp() {
		return this.lastUpdatedTimestamp;
	}

	public void setLastUpdatedTimestamp(Timestamp lastUpdatedTimestamp) {
		this.lastUpdatedTimestamp = lastUpdatedTimestamp;
	}

	public String getLastUpdatedUser() {
		return this.lastUpdatedUser;
	}

	public void setLastUpdatedUser(String lastUpdatedUser) {
		this.lastUpdatedUser = lastUpdatedUser;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public List<CustomerAccountRel> getCustomerAccountRels() {
		return this.customerAccountRels;
	}

	public void setCustomerAccountRels(List<CustomerAccountRel> customerAccountRels) {
		this.customerAccountRels = customerAccountRels;
	}

	public CustomerAccountRel addCustomerAccountRel(CustomerAccountRel customerAccountRel) {
		getCustomerAccountRels().add(customerAccountRel);
		customerAccountRel.setAccountRelationTypeMapping(this);

		return customerAccountRel;
	}

	public CustomerAccountRel removeCustomerAccountRel(CustomerAccountRel customerAccountRel) {
		getCustomerAccountRels().remove(customerAccountRel);
		customerAccountRel.setAccountRelationTypeMapping(null);

		return customerAccountRel;
	}

}