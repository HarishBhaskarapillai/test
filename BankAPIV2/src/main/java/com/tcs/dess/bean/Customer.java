package com.tcs.dess.bean;

import java.io.Serializable;

import javax.persistence.*;

import org.hibernate.annotations.Cascade;

import com.fasterxml.jackson.annotation.JsonFilter;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the customer database table.
 * 
 */
@Entity
@JsonFilter("BANKAPI")
//@JsonIdentityInfo(generator=ObjectIdGenerators.IntSequenceGenerator.class, property="customerId")
@NamedQuery(name="Customer.findAll", query="SELECT c FROM Customer c")
public class Customer implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="customer_id")
	private Long customerId;

	@Column(name="first_name")
	private String firstName;
	
	@Column(name="last_name")
	private String lastName;
	
	@Column(name="middle_name")
	private String middleName;
	
	private String prefix;

	private String suffix;
	
	@Temporal(TemporalType.DATE)
	private Date dob;
	
	private String gender;
	

	@Column(name="marrital_status")
	private String maritalStatus;
	
	//bi-directional many-to-one association to CountryMapping
	@ManyToOne
	@JoinColumn(name="citizenship")
	private CountryMapping countryMapping;
	
	
	//bi-directional many-to-one association to Contact
	
	
	@OneToMany(mappedBy="customer",   fetch=FetchType.LAZY, cascade=CascadeType.ALL)	
	private List<Contact> contacts;	

	//bi-directional many-to-one association to Identification
	@OneToMany(mappedBy="customer", cascade=CascadeType.ALL)
	private List<Identification> identifications;
	
	//bi-directional many-to-one association to CustomerAddressRel
	@OneToMany(mappedBy="customer")
	private List<CustomerAddressRel> customerAddressRels;

	//bi-directional many-to-one association to CustomerAccountRel
	@OneToMany(mappedBy="customer")
	private List<CustomerAccountRel> customerAccountRels;


	@Column(name="created_timestamp")
	private Timestamp createdTimestamp;

	@Column(name="created_user")
	private String createdUser;


	@Column(name="last_updated_timestamp")
	private Timestamp lastUpdatedTimestamp;

	@Column(name="last_updated_user")
	private String lastUpdatedUser;
	
	public Customer() {
	}

	public Long getCustomerId() {
		return this.customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public Timestamp getCreatedTimestamp() {
		return this.createdTimestamp;
	}

	public void setCreatedTimestamp(Timestamp createdTimestamp) {
		this.createdTimestamp = createdTimestamp;
	}

	public String getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(String createdUser) {
		this.createdUser = createdUser;
	}

	public Date getDob() {
		return this.dob;
	}

	public void setDob(Date dob) {
		this.dob = dob;
	}

	public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getGender() {
		return this.gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getLastName() {
		return this.lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Timestamp getLastUpdatedTimestamp() {
		return this.lastUpdatedTimestamp;
	}

	public void setLastUpdatedTimestamp(Timestamp lastUpdatedTimestamp) {
		this.lastUpdatedTimestamp = lastUpdatedTimestamp;
	}

	public String getLastUpdatedUser() {
		return this.lastUpdatedUser;
	}

	public void setLastUpdatedUser(String lastUpdatedUser) {
		this.lastUpdatedUser = lastUpdatedUser;
	}

	public String getMaritalStatus() {
		return this.maritalStatus;
	}

	public void setMaritalStatus(String maritalStatus) {
		this.maritalStatus = maritalStatus;
	}

	public String getMiddleName() {
		return this.middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getPrefix() {
		return this.prefix;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	public String getSuffix() {
		return this.suffix;
	}

	public void setSuffix(String suffix) {
		this.suffix = suffix;
	}

	public List<Contact> getContacts() {
		return this.contacts;
	}

	public void setContacts(List<Contact> contacts) {
		for (int i=0; i<contacts.size(); i++) {
			contacts.get(i).setCustomer(this);
		}
		this.contacts = contacts;
	}

	public Contact addContact(Contact contact) {
		getContacts().add(contact);
		contact.setCustomer(this);
		

		return contact;
	}

	public Contact removeContact(Contact contact) {
		getContacts().remove(contact);
		contact.setCustomer(null);

		return contact;
	}

	public CountryMapping getCountryMapping() {
		return this.countryMapping;
	}

	public void setCountryMapping(CountryMapping countryMapping) {
		this.countryMapping = countryMapping;
	}

	public List<CustomerAccountRel> getCustomerAccountRels() {
		return this.customerAccountRels;
	}

	public void setCustomerAccountRels(List<CustomerAccountRel> customerAccountRels) {
		for (int i=0; i<customerAccountRels.size(); i++) {
			customerAccountRels.get(i).setCustomer(this);
		}
		this.customerAccountRels = customerAccountRels;
	}

	public CustomerAccountRel addCustomerAccountRel(CustomerAccountRel customerAccountRel) {
		getCustomerAccountRels().add(customerAccountRel);
		customerAccountRel.setCustomer(this);

		return customerAccountRel;
	}

	public CustomerAccountRel removeCustomerAccountRel(CustomerAccountRel customerAccountRel) {
		getCustomerAccountRels().remove(customerAccountRel);
		customerAccountRel.setCustomer(null);

		return customerAccountRel;
	}

	public List<CustomerAddressRel> getCustomerAddressRels() {
		return this.customerAddressRels;
	}

	public void setCustomerAddressRels(List<CustomerAddressRel> customerAddressRels) {
		for (int i=0; i<customerAddressRels.size(); i++) {
			customerAddressRels.get(i).setCustomer(this);
		}
		this.customerAddressRels = customerAddressRels;
	}

	public CustomerAddressRel addCustomerAddressRel(CustomerAddressRel customerAddressRel) {
		getCustomerAddressRels().add(customerAddressRel);
		customerAddressRel.setCustomer(this);

		return customerAddressRel;
	}

	public CustomerAddressRel removeCustomerAddressRel(CustomerAddressRel customerAddressRel) {
		getCustomerAddressRels().remove(customerAddressRel);
		customerAddressRel.setCustomer(null);

		return customerAddressRel;
	}

	public List<Identification> getIdentifications() {
		return this.identifications;
	}

	public void setIdentifications(List<Identification> identifications) {
		for (int i=0; i<identifications.size(); i++) {
			identifications.get(i).setCustomer(this);
		}
		this.identifications = identifications;
	}

	public Identification addIdentification(Identification identification) {
		getIdentifications().add(identification);
		identification.setCustomer(this);

		return identification;
	}

	public Identification removeIdentification(Identification identification) {
		getIdentifications().remove(identification);
		identification.setCustomer(null);

		return identification;
	}

}