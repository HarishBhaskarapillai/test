/**
 * 
 * SessionUtil.java 
 *
 * @author TCS
 * @Version 1.0 - 2016
 * 
 * @Copyright 2016 Tata Consultancy 
 */
package com.tcs.dess.utils;

/**
 * This StringUtils a utility class for handling strings
 * 
 */
public class StringUtils {

	public static boolean isEmpty(String value) {
		if (value == null || value.trim().length() == 0)
			 return true;
		else 
			return false;
	}
}
