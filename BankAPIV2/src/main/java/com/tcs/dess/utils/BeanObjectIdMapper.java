/**
 * 
 * UserService.java 
 *
 * @author TCS
 * @Version 1.0 - 2016
 * 
 * @Copyright 2016 Tata Consultancy 
 */
package com.tcs.dess.utils;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class contains the Bean Object Id mapping to support JsonIdentityInfo during serialization/de-serialization 
 * when user requests for specific fields
 *
 */

public class BeanObjectIdMapper {

	private static final Logger logger = LoggerFactory.getLogger(BeanObjectIdMapper.class);

	private static final Map<String, String> OBJECT_ID_MAP;

	static {		
		Map<String, String> map = new HashMap<String, String>();
		map.put("usert", "userId");
		OBJECT_ID_MAP = Collections.unmodifiableMap(map);
	}
	
	public static String getObjectId(String key) {
		String objectId = null;
		if (key == null) 
			return null;

		logger.debug("Key: " + key);

		if (key.endsWith("T") || key.endsWith("Ts")) {
			if (key.endsWith("Ts")) {
				key = key.substring(0, key.length()-1);
			}

			String keyStr = key.toLowerCase(); 
			if (OBJECT_ID_MAP.containsKey(keyStr))
				objectId =  OBJECT_ID_MAP.get(keyStr);
		}
		return objectId;
	}
}
