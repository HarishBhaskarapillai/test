/**
 * 
 */
package com.tcs.dess.data.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.tcs.dess.bean.CustomerAccountRel;

/**
 * @author PocCoe
 *
 */
@Repository
public interface CustomerAccountRelRepository extends CrudRepository<CustomerAccountRel, Long>{

	/**
	 * @param customerId
	 * @return
	 */
	List<CustomerAccountRel> findByCustomerId(Long customerId);
 
	/**
	 * @param accountId
	 * @return
	 */
	CustomerAccountRel findByAccountId(Long accountId); 

}
