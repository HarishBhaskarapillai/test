/**
 * 
 * UserService.java 
 *
 * @author TCS
 * @Version 1.0 - 2016
 * 
 * @Copyright 2016 Tata Consultancy 
 */
package com.tcs.dess.service;

import java.sql.Timestamp;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tcs.dess.bean.LoginHistoryT;
import com.tcs.dess.bean.User;
import com.tcs.dess.bean.UserT;
import com.tcs.dess.data.repository.LoginHistoryRepository;
import com.tcs.dess.data.repository.UserRepository;

/**
 * 
 * This service handles functionalities related to user such as find, last login, login history
 * privileges and forgot password
 *
 */
@Service
public class UserService {

	private static final Logger logger = LoggerFactory
			.getLogger(UserService.class);
	
	@Autowired
	private LoginHistoryRepository loginHistoryRepository;
	
	@Autowired
	private UserRepository userRepository;
	
	/**
	 * This method is used to retrieve user details based on userId
	 * @param userId
	 * @return
	 * @throws Exception
	 */
	public User findByUserId(Long userId) throws Exception {
		logger.debug("Begin:Inside findByUserId() service");
		User dbUser = userRepository.findOne(userId);
		logger.debug("End:Inside findByUserId() service");
		return dbUser;
	}

	/**
	 * This method is used to find user login details for the given session id.
	 * 
	 * @param userId
	 *            , sessionId.
	 * @return user login details.
	 */
	public LoginHistoryT findByUserIdAndSessionId(Long userId,
			String sessionId) throws Exception {
		logger.debug("Begin:Inside findByUserIdAndSessionId() of Userservice");
		LoginHistoryT loginHistory = null;
		if (userId != null && sessionId != null) {
			loginHistory = loginHistoryRepository.findByUserIdAndSessionId(
					userId, sessionId);
		}
		logger.debug("End:Inside findByUserIdAndSessionId() of Userservice");
		return loginHistory;
	}
	
	/**
	 * This method is used to find the last login details of a specific user
	 * @param userId
	 * @return
	 */
//	public Timestamp getUserLastLogin(Long userId) {
//		logger.debug("Begin:Inside getUserLastLogin of UserService");
//		Timestamp lastLogin = null;
//		LoginHistoryT loginHistory = loginHistoryRepository
//				.findLastLoginByUserId(userId);
//		if (loginHistory != null)
//			lastLogin = loginHistory.getLoginDatetime();
//		logger.debug("End:Inside getUserLastLogin of UserService");
//		return lastLogin;
//	}
//	

	/**
	 * This method is used to add login history
	 * @param loginHistory
	 * @return
	 */

	public boolean addLoginHistory(LoginHistoryT loginHistory) {
		logger.debug("Begin:Inside addLoginHistory() of Userservice");
		LoginHistoryT managedLoginHistory = loginHistoryRepository
				.save(loginHistory);
		if (managedLoginHistory == null){
			logger.debug("End:Inside addLoginHistory() of Userservice");
			return false;
		}
		else{
			logger.debug("End:Inside addLoginHistory() of Userservice");
			return true;
		}
	}

	public User findByUserName(String userName) throws Exception {
		logger.debug("Begin:Inside findByUserName() service");
		User dbUser = userRepository.findByUserName(userName);
		logger.debug("End:Inside findByUserName() service");
		return dbUser;
	}
	
	/**
	 * @param userId
	 * @param password
	 * @return
	 */
	public User findByUserNameAndPassword(String userName, String password) {
		User foundUser = userRepository.findByUserNameAndPassword(userName,password);
		if(foundUser==null)
		{
			foundUser = new User();
			foundUser.setUserId(Long.valueOf(0));
		}
		return foundUser;
	}

	/**
	 * @param user
	 */
	public void save(User user) {
		userRepository.save(user);	
	}
	
}