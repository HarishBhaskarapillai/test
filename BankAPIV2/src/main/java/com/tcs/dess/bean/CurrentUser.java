/**
 * 
 */
package com.tcs.dess.bean;

import org.springframework.security.core.authority.AuthorityUtils;

/**
 * @author PocCoe
 *
 */
public class CurrentUser extends org.springframework.security.core.userdetails.User {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private User user;

    public CurrentUser(User user) {
        super(user.getUserName(), user.getPassword(), AuthorityUtils.createAuthorityList(user.getRole()));
        this.user = user;
    }

    public User getUser() {
        return user;
    }

    public String getRole() {
        return user.getRole();
    }

}