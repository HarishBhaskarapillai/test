package com.tcs.dess.bean;

import java.io.Serializable;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonFilter;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the address database table.
 * 
 */
@Entity
@JsonFilter("BANKAPI")
//@JsonIdentityInfo(generator=ObjectIdGenerators.IntSequenceGenerator.class, property="addressId")
@NamedQuery(name="Address.findAll", query="SELECT a FROM Address a")
public class Address implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="address_id")
	private Long addressId;

	@Column(name="address_line_one")
	private String addressLineOne;

	@Column(name="address_line_three")
	private String addressLineThree;

	@Column(name="address_line_two")
	private String addressLineTwo;

	private String city;

	
//	@Column(name="state")
//	private String state;

	//bi-directional many-to-one association to StateMapping
	@ManyToOne
	@JoinColumn(name="state")
	private StateMapping stateMapping;
	
	
	//bi-directional many-to-one association to CountryMapping
		@ManyToOne
		@JoinColumn(name="country")
		private CountryMapping countryMapping;
		
		private String zipcode;
		
		//bi-directional many-to-one association to AddressTypeMapping
		@ManyToOne
		@JoinColumn(name="address_type")
		private AddressTypeMapping addressTypeMapping;
		
	@Column(name="created_timestamp")
	private Timestamp createdTimestamp;

	@Column(name="created_user")
	private String createdUser;

	@Column(name="last_updated_timestamp")
	private Timestamp lastUpdatedTimestamp;

	@Column(name="last_updated_user")
	private String lastUpdatedUser;



	//bi-directional many-to-one association to CustomerAddressRel
	@JsonIgnore
	@OneToMany(mappedBy="address", cascade=CascadeType.ALL)
	private List<CustomerAddressRel> customerAddressRels;

	public Address() {
	}

	public Long getAddressId() {
		return this.addressId;
	}

	public void setAddressId(Long addressId) {
		this.addressId = addressId;
	}

	public String getAddressLineOne() {
		return this.addressLineOne;
	}

	public void setAddressLineOne(String addressLineOne) {
		this.addressLineOne = addressLineOne;
	}

	public String getAddressLineThree() {
		return this.addressLineThree;
	}

	public void setAddressLineThree(String addressLineThree) {
		this.addressLineThree = addressLineThree;
	}

	public String getAddressLineTwo() {
		return this.addressLineTwo;
	}

	public void setAddressLineTwo(String addressLineTwo) {
		this.addressLineTwo = addressLineTwo;
	}

	public String getCity() {
		return this.city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public Timestamp getCreatedTimestamp() {
		return this.createdTimestamp;
	}

	public void setCreatedTimestamp(Timestamp createdTimestamp) {
		this.createdTimestamp = createdTimestamp;
	}

	public String getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(String createdUser) {
		this.createdUser = createdUser;
	}

	public Timestamp getLastUpdatedTimestamp() {
		return this.lastUpdatedTimestamp;
	}

	public void setLastUpdatedTimestamp(Timestamp lastUpdatedTimestamp) {
		this.lastUpdatedTimestamp = lastUpdatedTimestamp;
	}

	public String getLastUpdatedUser() {
		return this.lastUpdatedUser;
	}

	public void setLastUpdatedUser(String lastUpdatedUser) {
		this.lastUpdatedUser = lastUpdatedUser;
	}

	public String getZipcode() {
		return this.zipcode;
	}

	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}

	public AddressTypeMapping getAddressTypeMapping() {
		return this.addressTypeMapping;
	}

	public void setAddressTypeMapping(AddressTypeMapping addressTypeMapping) {
		this.addressTypeMapping = addressTypeMapping;
	}

	public CountryMapping getCountryMapping() {
		return this.countryMapping;
	}

	public void setCountryMapping(CountryMapping countryMapping) {
		this.countryMapping = countryMapping;
	}

	public StateMapping getStateMapping() {
		return this.stateMapping;
	}

	public void setStateMapping(StateMapping stateMapping) {
		this.stateMapping = stateMapping;
	}

	public List<CustomerAddressRel> getCustomerAddressRels() {
		return this.customerAddressRels;
	}

	public void setCustomerAddressRels(List<CustomerAddressRel> customerAddressRels) {
		for (int i=0; i<customerAddressRels.size(); i++) {
			customerAddressRels.get(i).setAddress(this);
		}
		this.customerAddressRels = customerAddressRels;
	}

	public CustomerAddressRel addCustomerAddressRel(CustomerAddressRel customerAddressRel) {
		getCustomerAddressRels().add(customerAddressRel);
		customerAddressRel.setAddress(this);

		return customerAddressRel;
	}

	public CustomerAddressRel removeCustomerAddressRel(CustomerAddressRel customerAddressRel) {
		getCustomerAddressRels().remove(customerAddressRel);
		customerAddressRel.setAddress(null);

		return customerAddressRel;
	}

}