package com.tcs.dess.bean;

import java.io.Serializable;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonFilter;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the state_mapping database table.
 * 
 */
@Entity
@JsonFilter("BANKAPI")
//@JsonIdentityInfo(generator=ObjectIdGenerators.IntSequenceGenerator.class, property="stateId")
@Table(name="state_mapping")
@NamedQuery(name="StateMapping.findAll", query="SELECT s FROM StateMapping s")
public class StateMapping implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="state_id")
	private Long stateId;

	private String name;

	@Column(name="state_code")
	private String stateCode;
	
	@Column(name="created_timestamp")
	private Timestamp createdTimestamp;

	@Column(name="created_user")
	private String createdUser;

	@Column(name="last_updated_timestamp")
	private Timestamp lastUpdatedTimestamp;

	@Column(name="last_updated_user")
	private String lastUpdatedUser;

	

	//bi-directional many-to-one association to Address
	@JsonIgnore
	@OneToMany(mappedBy="stateMapping",fetch=FetchType.LAZY)
	private List<Address> addresses;

	//bi-directional many-to-one association to BranchCodeMapping
	@JsonIgnore
	@OneToMany(mappedBy="stateMapping",fetch=FetchType.LAZY)
	private List<BranchCodeMapping> branchCodeMappings;

	public StateMapping() {
	}

	public Long getStateId() {
		return this.stateId;
	}

	public void setStateId(Long stateId) {
		this.stateId = stateId;
	}

	public Timestamp getCreatedTimestamp() {
		return this.createdTimestamp;
	}

	public void setCreatedTimestamp(Timestamp createdTimestamp) {
		this.createdTimestamp = createdTimestamp;
	}

	public String getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(String createdUser) {
		this.createdUser = createdUser;
	}

	public Timestamp getLastUpdatedTimestamp() {
		return this.lastUpdatedTimestamp;
	}

	public void setLastUpdatedTimestamp(Timestamp lastUpdatedTimestamp) {
		this.lastUpdatedTimestamp = lastUpdatedTimestamp;
	}

	public String getLastUpdatedUser() {
		return this.lastUpdatedUser;
	}

	public void setLastUpdatedUser(String lastUpdatedUser) {
		this.lastUpdatedUser = lastUpdatedUser;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getStateCode() {
		return this.stateCode;
	}

	public void setStateCode(String stateCode) {
		this.stateCode = stateCode;
	}

	public List<Address> getAddresses() {
		return this.addresses;
	}

	public void setAddresses(List<Address> addresses) {
		this.addresses = addresses;
	}

	public Address addAddress(Address address) {
		getAddresses().add(address);
		address.setStateMapping(this);

		return address;
	}

	public Address removeAddress(Address address) {
		getAddresses().remove(address);
		address.setStateMapping(null);

		return address;
	}

	public List<BranchCodeMapping> getBranchCodeMappings() {
		return this.branchCodeMappings;
	}

	public void setBranchCodeMappings(List<BranchCodeMapping> branchCodeMappings) {
		this.branchCodeMappings = branchCodeMappings;
	}

	public BranchCodeMapping addBranchCodeMapping(BranchCodeMapping branchCodeMapping) {
		getBranchCodeMappings().add(branchCodeMapping);
		branchCodeMapping.setStateMapping(this);

		return branchCodeMapping;
	}

	public BranchCodeMapping removeBranchCodeMapping(BranchCodeMapping branchCodeMapping) {
		getBranchCodeMappings().remove(branchCodeMapping);
		branchCodeMapping.setStateMapping(null);

		return branchCodeMapping;
	}

}