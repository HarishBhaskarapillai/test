package com.tcs.dess.bean;

import java.io.Serializable;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonFilter;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import java.sql.Timestamp;


/**
 * The persistent class for the customer_address_rel database table.
 * 
 */
@Entity
@JsonFilter("BANKAPI")
//@JsonIdentityInfo(generator=ObjectIdGenerators.IntSequenceGenerator.class, property="customerAddressRelId")
@Table(name="customer_address_rel")
@NamedQuery(name="CustomerAddressRel.findAll", query="SELECT c FROM CustomerAddressRel c")
public class CustomerAddressRel implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="customer_address_rel_id")
	private Long customerAddressRelId;

	//bi-directional many-to-one association to Address
	@ManyToOne
	@JoinColumn(name="address_id")
	private Address address;
	
	@Column(name="created_timestamp")
	private Timestamp createdTimestamp;

	@Column(name="created_user")
	private String createdUser;

	@Column(name="last_updated_timestamp")
	private Timestamp lastUpdatedTimestamp;

	@Column(name="last_updated_user")
	private String lastUpdatedUser;



	//bi-directional many-to-one association to Customer
	@JsonIgnore
	@ManyToOne
	@JoinColumn(name="customer_id")
	private Customer customer;

	public CustomerAddressRel() {
	}

	public Long getCustomerAddressRelId() {
		return this.customerAddressRelId;
	}

	public void setCustomerAddressRelId(Long customerAddressRelId) {
		this.customerAddressRelId = customerAddressRelId;
	}

	public Timestamp getCreatedTimestamp() {
		return this.createdTimestamp;
	}

	public void setCreatedTimestamp(Timestamp createdTimestamp) {
		this.createdTimestamp = createdTimestamp;
	}

	public String getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(String createdUser) {
		this.createdUser = createdUser;
	}

	public Timestamp getLastUpdatedTimestamp() {
		return this.lastUpdatedTimestamp;
	}

	public void setLastUpdatedTimestamp(Timestamp lastUpdatedTimestamp) {
		this.lastUpdatedTimestamp = lastUpdatedTimestamp;
	}

	public String getLastUpdatedUser() {
		return this.lastUpdatedUser;
	}

	public void setLastUpdatedUser(String lastUpdatedUser) {
		this.lastUpdatedUser = lastUpdatedUser;
	}

	public Address getAddress() {
		return this.address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public Customer getCustomer() {
		return this.customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

}