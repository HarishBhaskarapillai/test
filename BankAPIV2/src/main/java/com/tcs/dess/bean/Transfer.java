package com.tcs.dess.bean;

import java.io.Serializable;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonFilter;

import java.sql.Timestamp;
import java.math.BigDecimal;


/**
 * The persistent class for the transfer database table.
 * 
 */
@Entity
@JsonFilter("BANKAPI")
@NamedQuery(name="Transfer.findAll", query="SELECT t FROM Transfer t")
public class Transfer implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="transfer_id")
	private Long transferId;

	@Column(name="created_timestamp")
	private Timestamp createdTimestamp;

	@Column(name="created_user")
	private String createdUser;

	@Column(name="last_updated_timestamp")
	private Timestamp lastUpdatedTimestamp;

	@Column(name="last_updated_user")
	private String lastUpdatedUser;

	@Column(name="transfer_amount")
	private BigDecimal transferAmount;

	@Column(name="transfer_date")
	private Timestamp transferDate;

	@Column(name="transfer_type")
	private String transferType;

	//bi-directional many-to-one association to Account
	@ManyToOne
	@JoinColumn(name="from_account_id")
	private Account account;
	
	
	@Column(name="from_account_id",insertable=false, updatable=false)
	private Long accountId;

	/**
	 * @return the accountId
	 */
	public Long getAccountId() {
		return accountId;
	}

	/**
	 * @param accountId the accountId to set
	 */
	public void setAccountId(Long accountId) {
		this.accountId = accountId;
	}

	//bi-directional many-to-one association to Payee
	@ManyToOne
	@JoinColumn(name="payee_id")
	private Payee payee;

	public Transfer() {
	}

	public Long getTransferId() {
		return this.transferId;
	}

	public void setTransferId(Long transferId) {
		this.transferId = transferId;
	}

	public Timestamp getCreatedTimestamp() {
		return this.createdTimestamp;
	}

	public void setCreatedTimestamp(Timestamp createdTimestamp) {
		this.createdTimestamp = createdTimestamp;
	}

	public String getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(String createdUser) {
		this.createdUser = createdUser;
	}

	public Timestamp getLastUpdatedTimestamp() {
		return this.lastUpdatedTimestamp;
	}

	public void setLastUpdatedTimestamp(Timestamp lastUpdatedTimestamp) {
		this.lastUpdatedTimestamp = lastUpdatedTimestamp;
	}

	public String getLastUpdatedUser() {
		return this.lastUpdatedUser;
	}

	public void setLastUpdatedUser(String lastUpdatedUser) {
		this.lastUpdatedUser = lastUpdatedUser;
	}

	public BigDecimal getTransferAmount() {
		return this.transferAmount;
	}

	public void setTransferAmount(BigDecimal transferAmount) {
		this.transferAmount = transferAmount;
	}

	public Timestamp getTransferDate() {
		return this.transferDate;
	}

	public void setTransferDate(Timestamp transferDate) {
		this.transferDate = transferDate;
	}

	public String getTransferType() {
		return this.transferType;
	}

	public void setTransferType(String transferType) {
		this.transferType = transferType;
	}

	public Account getAccount() {
		return this.account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}

	public Payee getPayee() {
		return this.payee;
	}

	public void setPayee(Payee payee) {
		this.payee = payee;
	}

}