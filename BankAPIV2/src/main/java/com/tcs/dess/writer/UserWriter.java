package com.tcs.dess.writer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.WriteListener;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemWriter;

import com.tcs.dess.bean.User;
import com.tcs.dess.bean.UserT;
import com.tcs.dess.data.repository.UserRepository;
import com.tcs.dess.enums.Operation;

public class UserWriter implements ItemWriter<String[]>, WriteListener {

	private static final Logger logger = LoggerFactory
			.getLogger(UserWriter.class);


	private UserRepository userRepository;

	List<User> userList = new ArrayList<User>();

	@Override
	public void write(List<? extends String[]> items) throws Exception {
		
		logger.debug("Inside write:");

		String operation = null; 
		for (String[] data: items) {
			operation = (String) data[1];
			if(operation!=null) {
				if (operation.equalsIgnoreCase(Operation.ADD.name())) {
					logger.debug("***USER ADD***");
					User user = populateUser(data);
					userList.add(user);
				}
					
			}
		}
		
		if ((CollectionUtils.isNotEmpty(userList))) {

			if (operation.equalsIgnoreCase(Operation.ADD.name())) {
				userRepository.save(userList);
			} 
		}
	}


	/**
	 * @param data
	 * @return
	 */
	private User populateUser(String[] data) {
		
		User user = new User();
		user.setUserId(Long.valueOf(data[2]));
		user.setUserName(data[3]);
		user.setPassword(data[4]);
		user.setRole(data[5]);
		return user;
	}


	public UserRepository getUserRepository() {
		return userRepository;
	}

	public void setUserRepository(UserRepository userRepository) {
		this.userRepository = userRepository;
	}


	/* (non-Javadoc)
	 * @see javax.servlet.WriteListener#onError(java.lang.Throwable)
	 */
	@Override
	public void onError(Throwable arg0) {
		logger.error("Error occured during write" + arg0.getMessage());
		
	}


	/* (non-Javadoc)
	 * @see javax.servlet.WriteListener#onWritePossible()
	 */
	@Override
	public void onWritePossible() throws IOException {
		// TODO Auto-generated method stub
		
	}

}
