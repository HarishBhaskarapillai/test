/**
 * 
 * DataProcessingController.java 
 *
 * @author TCS
 * @Version 1.0 - 2016
 * 
 * @Copyright 2016 Tata Consultancy 
 */
package com.tcs.dess.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.tcs.dess.bean.RequestVO;
import com.tcs.dess.bean.User;
import com.tcs.dess.bean.UserT;
import com.tcs.dess.enums.Status;
import com.tcs.dess.exception.MobilityException;
import com.tcs.dess.service.DataProcessingService;
import com.tcs.dess.utils.ResponseConstructors;
import com.tcs.dess.utils.SessionUtil;

/**
 * Controller to handle batch requests.
 * 
 */
/**
 * This DataProcessingController class holds services for handling data
 * 
 */
@RestController
@RequestMapping("/data")
public class DataProcessingController {

	private static final Logger logger = LoggerFactory
			.getLogger(DataProcessingController.class);

	@Autowired
	private DataProcessingService service;

	@Autowired
	JobLauncherController jobLauncherController;

	/**
	 * This method is used to request upload to database
	 * 
	 * @param file
	 * @param type
	 * @param fields
	 * @param view
	 * @return ResponseEntity
	 * @throws Exception
	 */
	@RequestMapping(value = "/upload", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<String> batchUploadRequest(
			@RequestParam("file") MultipartFile file,
			@RequestParam(value = "deleteFrom", defaultValue = "") String deleteFrom,
			@RequestParam(value = "deleteTo", defaultValue = "") String deleteTo,
			@RequestParam(value = "fields", defaultValue = "all") String fields,
			@RequestParam(value = "view", defaultValue = "") String view)
			throws Exception {

		logger.info("Inside Data Processing controller: Start of upload");

		Status status = null;
		String description = null;
		try {
			User user = SessionUtil.getCurrentUserDetails();
			RequestVO request = service.processUploadRequest(file, user);
			status = jobLauncherController.asyncJobLaunch(request);
			if (status.getStatus().equals(Status.SUCCESS)) {
				description = "User upload started!";
			} else {
				description = "Internal Server error, during the upload process";
			}
			status.setStatus(status.getStatus(), description);
			logger.info(description);
			
		} catch (MobilityException e) {
			logger.error("Destination Exception" + e.getMessage());
			throw e;
		} catch (Exception e) {
			logger.error("INTERNAL_SERVER_ERROR" + e.getMessage());
			throw new MobilityException(HttpStatus.INTERNAL_SERVER_ERROR,
					e.getMessage());
		}
		logger.info("Inside Data Processing controller: End of upload");
		return new ResponseEntity<String>(
				ResponseConstructors.filterJsonForFieldAndViews("","",
						status), HttpStatus.OK);
	}

}
