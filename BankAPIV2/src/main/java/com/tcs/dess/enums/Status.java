/**
 * 
 * Status.java 
 *
 * @author TCS
 * @Version 1.0 - 2016
 * 
 * @Copyright 2016 Tata Consultancy 
 */
package com.tcs.dess.enums;

/**
 * This Status enum holds the details for the status of the services
 * 
 */
public class Status {

	public static final String SUCCESS = "Success";

	public static final String FAILED = "Failed";

	private String status;

	private String description;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status, String description) {
		this.status = status;
		this.description = description;
	}

	public String getDescription() {
		return description;
	}
}