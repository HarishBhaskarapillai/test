/**
 * 
 * UserRepositoryUserDetailsService.java 
 *
 * @author TCS
 * @Version 1.0 - 2016
 * 
 * @Copyright 2016 Tata Consultancy 
 */
package com.tcs.dess.controller;

import static com.tcs.dess.utils.Constants.BAD_CREDENTIALS_MSG;

import java.util.Collection;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.tcs.dess.bean.User;
import com.tcs.dess.bean.UserT;
import com.tcs.dess.data.repository.UserRepository;
import com.tcs.dess.exception.MobilityException;
import com.tcs.dess.utils.PropertyUtil;

/**
 * This service queries the userRepository and have its appropriate getters and
 * setters
 */
@Service
public class UserRepositoryUserDetailsService implements UserDetailsService {

	private static final Logger logger = LoggerFactory
			.getLogger(UserRepositoryUserDetailsService.class);

	@Autowired
	private UserRepository userRepository;

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.security.core.userdetails.UserDetailsService#
	 * loadUserByUsername(java.lang.String)
	 */
	@Override
	public UserDetails loadUserByUsername(String userName)
			throws UsernameNotFoundException {
		User user = null;

		try {
			logger.info("Inside user detail service");
			logger.info(userName);
			user = userRepository.findByUserName(userName);
		} catch (EmptyResultDataAccessException e) {
			e.printStackTrace();
			logger.info("No Such User: " + userName);
			throw new MobilityException(HttpStatus.UNAUTHORIZED,
					PropertyUtil.getProperty(BAD_CREDENTIALS_MSG));
		} catch (IncorrectResultSizeDataAccessException e) {
			logger.error("More than one user found for the user name: "
					+ userName);
			e.printStackTrace();
			throw new MobilityException(HttpStatus.INTERNAL_SERVER_ERROR,
					"More than one user found for the user name: " + userName);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e.getMessage());
			throw new MobilityException(HttpStatus.INTERNAL_SERVER_ERROR,
					e.getMessage());
		}

//		if (user == null) {
//			logger.error("User not authorized: " + userName);
//			throw new MobilityException(HttpStatus.UNAUTHORIZED,
//					PropertyUtil.getProperty(BAD_CREDENTIALS_MSG));
//		}
		System.out.println("last");
		return new UserRepositoryUserDetails(user);
	}

	/**
	 * UserRepositoryUserDetails and its getters and setters
	 *
	 */
	public final static class UserRepositoryUserDetails extends User implements
			UserDetails {

		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		private UserRepositoryUserDetails(User user) {
			super(user);
		}

		@Override
		public Collection<? extends GrantedAuthority> getAuthorities() {
			return null;

		}

		@Override
		public String getUsername() {
			return getUserName();
		}
		
		@Override
		public boolean isAccountNonExpired() {
			return true;
		}

		@Override
		public boolean isAccountNonLocked() {
			return true;
		}

		@Override
		public boolean isCredentialsNonExpired() {
			return true;
		}

		@Override
		public boolean isEnabled() {
			return true;
		}
	}
}