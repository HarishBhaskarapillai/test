/**
 * 
 * TransactionController.java 
 *
 * @author TCS
 * @Version 1.0 - 2016
 * 
 * @Copyright 2016 Tata Consultancy 
 */
package com.tcs.dess.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ws.rs.PathParam;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.tcs.dess.bean.Customer;
import com.tcs.dess.bean.RequestVO;
import com.tcs.dess.bean.StatusResponse;
import com.tcs.dess.bean.Transaction;
import com.tcs.dess.bean.UserT;
import com.tcs.dess.enums.Status;
import com.tcs.dess.exception.MobilityException;
import com.tcs.dess.service.AccountService;
import com.tcs.dess.service.CustomerService;
import com.tcs.dess.service.TransactionService;
import com.tcs.dess.utils.Constants;
import com.tcs.dess.utils.ResponseConstructors;
import com.tcs.dess.utils.SessionUtil;

/**
 * This TransactionController class holds services for handling Transaction
 * 
 */
@RestController
@RequestMapping("/transactions")
public class TransactionController {

	private static final Logger logger = LoggerFactory
			.getLogger(TransactionController.class);

	@Autowired
	TransactionService transactionService;

	@RequestMapping(method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<StatusResponse> postTransaction(
			@RequestBody Transaction transaction) {

		String response = "";
		logger.info("Post Transaction Invoked");

		try {

			response = transactionService.postTransaction(transaction);
			if(response.equalsIgnoreCase(Constants.TRANSACTION_NOT_ENOUGH_BALANCE))
			{
				throw new MobilityException(HttpStatus.NOT_ACCEPTABLE,
						"Not enough Balance");
			}

		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new MobilityException(HttpStatus.INTERNAL_SERVER_ERROR,
					e.getMessage());
		}
		StatusResponse statusResponse=new StatusResponse();
		statusResponse.setMessage("TransactionPosted : Transaction Id : " + response);

		return new ResponseEntity<StatusResponse>(statusResponse,HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET,produces = "application/json")
	public @ResponseBody String getStatements(
			@RequestParam(value = "account_number", defaultValue = "") String accountNumber,
			@RequestParam(value = "account_type", defaultValue = "") String accountType,
			@RequestParam(value = "start_date", defaultValue = "") String startDate,
			@RequestParam(value = "end_date", defaultValue = "") String endDate,
			@RequestParam(value = "fields", defaultValue = "all") String fields,
			@RequestParam(value = "view", defaultValue = "all") String view) {

		List transactions = null;

		String response = "";

		try {
			if (accountNumber.equals("") || accountType.equals("")) {
				throw new MobilityException(HttpStatus.INTERNAL_SERVER_ERROR,
						"INVALID  INPUT");
			} else if (startDate.equals("") || endDate.equals("")) {
				transactions = transactionService.getMiniStatement(
						accountNumber, Long.parseLong(accountType));
			} else {				
				transactions = transactionService.getDetailedStatement(
						accountNumber, Long.parseLong(accountType), new Date(startDate), new Date(endDate));
			}

			logger.info("Response Size : "+transactions.size());
			
			fields = Constants.TRANSACTION_API_VIEW_GENERIC;
			  
			return ResponseConstructors.filterJsonForFieldAndViews(fields,
					view, transactions);

		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new MobilityException(HttpStatus.INTERNAL_SERVER_ERROR,
					e.getMessage());
		}
	}

}
