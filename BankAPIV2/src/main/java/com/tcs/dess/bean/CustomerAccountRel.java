package com.tcs.dess.bean;

import java.io.Serializable;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonFilter;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import java.sql.Timestamp;


/**
 * The persistent class for the customer_account_rel database table.
 * 
 */
@Entity
@JsonFilter("BANKAPI")
//@JsonIdentityInfo(generator=ObjectIdGenerators.IntSequenceGenerator.class, property="customerAccountRelId")
@Table(name="customer_account_rel")
@NamedQuery(name="CustomerAccountRel.findAll", query="SELECT c FROM CustomerAccountRel c")
public class CustomerAccountRel implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="customer_account_rel_id")
	private Long customerAccountRelId;

	//bi-directional many-to-one association to Account
	@ManyToOne
	@JoinColumn(name="account_id")
	private Account account;
	
	//bi-directional many-to-one association to AccountRelationTypeMapping
	@ManyToOne
	@JoinColumn(name="relation_type")
	private AccountRelationTypeMapping accountRelationTypeMapping;
	
	@Column(name="relation_type", insertable=false, updatable = false)
	private Long relationType;
	
	@Column(name="created_timestamp")
	private Timestamp createdTimestamp;

	/**
	 * @return the relationType
	 */
	public Long getRelationType() {
		return relationType;
	}

	/**
	 * @param relationType the relationType to set
	 */
	public void setRelationType(Long relationType) {
		this.relationType = relationType;
	}

	@Column(name="created_user")
	private String createdUser;

	@Column(name="last_updated_timestamp")
	private Timestamp lastUpdatedTimestamp;

	@Column(name="last_updated_user")
	private String lastUpdatedUser;





	//bi-directional many-to-one association to Customer
	@JsonIgnore
	@ManyToOne
	@JoinColumn(name="customer_id")
	private Customer customer;

	@Column(name="customer_id", insertable = false,updatable=false)
	private Long customerId;
	
	@Column(name="account_id", insertable = false,updatable=false)
	private Long accountId;
	
	public CustomerAccountRel() {
	}

	public Long getCustomerAccountRelId() {
		return this.customerAccountRelId;
	}

	public void setCustomerAccountRelId(Long customerAccountRelId) {
		this.customerAccountRelId = customerAccountRelId;
	}

	public Timestamp getCreatedTimestamp() {
		return this.createdTimestamp;
	}

	public void setCreatedTimestamp(Timestamp createdTimestamp) {
		this.createdTimestamp = createdTimestamp;
	}

	public String getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(String createdUser) {
		this.createdUser = createdUser;
	}

	public Timestamp getLastUpdatedTimestamp() {
		return this.lastUpdatedTimestamp;
	}

	public void setLastUpdatedTimestamp(Timestamp lastUpdatedTimestamp) {
		this.lastUpdatedTimestamp = lastUpdatedTimestamp;
	}

	public String getLastUpdatedUser() {
		return this.lastUpdatedUser;
	}

	public void setLastUpdatedUser(String lastUpdatedUser) {
		this.lastUpdatedUser = lastUpdatedUser;
	}

	public Account getAccount() {
		return this.account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}

	public AccountRelationTypeMapping getAccountRelationTypeMapping() {
		return this.accountRelationTypeMapping;
	}

	public void setAccountRelationTypeMapping(AccountRelationTypeMapping accountRelationTypeMapping) {
		this.accountRelationTypeMapping = accountRelationTypeMapping;
	}

	public Customer getCustomer() {
		return this.customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	/**
	 * @return the customerId
	 */
	public Long getCustomerId() {
		return customerId;
	}

	/**
	 * @param customerId the customerId to set
	 */
	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	/**
	 * @return the accountId
	 */
	public Long getAccountId() {
		return accountId;
	}

	/**
	 * @param accountId the accountId to set
	 */
	public void setAccountId(Long accountId) {
		this.accountId = accountId;
	}
	

}